#ifndef __XIL_IO__
#define __XIL_IO__


extern void Xil_Out32(uint32_t phyaddr, uint32_t val); 
extern int Xil_In32(uint32_t phyaddr);

#endif /*__XIL_IO__*/
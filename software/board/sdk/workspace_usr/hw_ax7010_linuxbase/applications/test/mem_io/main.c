#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
 
#include <unistd.h>
#include <fcntl.h>
#include "xil_io.h"
#include "usr_def.h"
 
//宏定义，字节对齐算法
//@x-表示字节数
//@a-表示多少个字节对齐
#define ALIGN(x, a)	(((x) + ((a) - 1)) & ~((a) - 1))

struct MEM_INFO_S mem_info_xx[]={
    {GPIO_ADDR_BASE+(0<<2),0x00000001},
    {GPIO_ADDR_BASE+(1<<2),0x00000020},
    {GPIO_ADDR_BASE+(2<<2),0x00000300},
    {GPIO_ADDR_BASE+(3<<2),0x00004000},
    {GPIO_ADDR_BASE+(4<<2),0x00050000},
    {GPIO_ADDR_BASE+(5<<2),0x00600000},
};
/**
read and write phy mem
 * */
/*写入一个地址*/
static void rio_setreg32(uint32_t addrBase,uint32_t addrOffset,uint32_t value)
{
	Xil_Out32(addrBase+addrOffset, value);
}
/*读取一个地址*/
static int rio_getreg32(uint32_t addrBase,uint32_t addrOffset)
{
	int ans=0;
	ans=Xil_In32(addrBase+addrOffset);
	return ans;
}
/*读取多个地址*/
static void mem_dump(uint32_t addrBase,uint32_t addrOffset,uint32_t len)
{
    uint32_t i;
    
    printf("Memory dump from 0x%08X ,size %d\n",addrBase+addrOffset,len);
    addrBase = addrBase+addrOffset;
    addrBase = ALIGN(addrBase,4);/*addr需要4byte对齐*/
    i =0;
    len = len/4;
    while(len--)
    {
        printf("R:0x%08X=[0x%08X];",addrBase+i*4,Xil_In32(addrBase+i*4));
        i++;
        if(0 == i%4)
        {
            printf("\n");
        }
    }
    printf("\n");
}

/*写入多个地址*/
static void mem_write_multi(struct MEM_INFO_S *pmem_info_xx,uint32_t len)
{
    uint32_t i;
    uint32_t addr,val;
    printf("mem_write_multi len=%d\n",len);    
    i =0;
    while(len--)
    {
        addr = pmem_info_xx[i].addr;
        addr = ALIGN(addr,4);/*addr需要4byte对齐*/
        val = pmem_info_xx[i].val;
        printf("W:0x%08X=[0x%08X];",addr,val);
        rio_setreg32(addr,0,val);
        i++;
        if(0 == i%4)
        {
            printf("\n");
        }
    }
    printf("\n");
}
int main(int argc, char *argv[])
{	
    uint32_t addrBase, addrOffset, len;
    struct MEM_INFO_S *pmem_info_write;
    printf("MMIO application\n");
    
    usleep(1000);//delay 1000 us,超过1秒请用sleep    
    sleep(1);//delay 1 second
    printf("sleep end...\n");
    /**************mem read test**********************/
    addrBase = MEM_ADDR_BASE;  //read memory
    addrOffset = 0;
    len = 16;
    mem_dump(addrBase,addrOffset,len);
    
    
    addrBase = SERIAL_ADDR_BASE;//read serial reg
    addrOffset = 0;
    len = 16;
    mem_dump(addrBase,addrOffset,len);
	
    
    addrBase = AD9226_ADDR_BASE;//read AD9226 reg
    addrOffset = 0;
    len = 16;
    mem_dump(addrBase,addrOffset,len);
    rio_setreg32(AD9226_ADDR_BASE,0,0x20);
    rio_setreg32(AD9226_ADDR_BASE,4,0x40);
    rio_setreg32(AD9226_ADDR_BASE,8,0x80);
    mem_dump(addrBase,addrOffset,len);
    /**************mem write test**********************/
    pmem_info_write = mem_info_xx;
    len = sizeof(mem_info_xx)/sizeof(struct MEM_INFO_S);
    mem_write_multi(pmem_info_write,len);
    
	return 0;
}

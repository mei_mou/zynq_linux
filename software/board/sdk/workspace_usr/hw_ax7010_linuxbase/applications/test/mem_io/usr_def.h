#ifndef __USR_DEF__
#define __USR_DEF__
#include <sys/types.h>

#define MEM_ADDR_BASE       (0x10000000)
#define GPIO_ADDR_BASE      (0xE000A000)
#define SERIAL_ADDR_BASE    (0XE0001000)
#define AD9226_ADDR_BASE  0x43c30000
struct MEM_INFO_S{
    uint32_t addr;
    uint32_t val;
};


#endif /*__USR_DEF__*/
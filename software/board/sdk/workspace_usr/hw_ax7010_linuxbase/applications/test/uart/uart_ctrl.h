
#ifndef __UART_CTRL_H__
#define __UART_CTRL_H__

#include "my_typedef.h"
#define DEVPATH_RCV_CMD  "/dev/ttyPS1"
int  uart_set_port_attr (
               int fd,
               int  baudrate,          // B1200 B2400 B4800 B9600 .. B115200
               int  databit,           // 5, 6, 7, 8
               const char *stopbit,    //  "1", "1.5", "2"
               char parity,            // N(o), O(dd), E(ven)
               int vtime,
               int vmin );
int init_dev_rcv_cmd(int *fd);

int uart_read(int fd, void *r_buf, int datalen);
int uart_write(int fd, void *w_buf, int datalen);
#endif /*__UART_CTRL_H__*/


/**************************************************************************
  * @brief        : linux环境下串口测试
  * @author       : shiweisun@foxmail.com
  * @copyright    : NONE
  * @version      : 0.1
  * @note         :   
  * @history      : 
***************************************************************************/
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<unistd.h>
#include<assert.h>
#include<termios.h>
#include<string.h>
#include<sys/time.h>
#include<sys/types.h>
#include<errno.h>
#include "my_typedef.h"
#include "uart_ctrl.h"
/******************define*******************************/


/******************gloable value*******************************/

int main(void)
{
    int err = 0;
    char w_buf[1024] = "something to write,hello welcome";
    size_t w_len = strlen(w_buf);
    int cnt;
    u8 rcv_buf[128];
    s32 rcv_len;
   
    int fd = 0;
    err = init_dev_rcv_cmd(&fd);
    if(err < 0)
    {
        goto erro;
    }
    //write test
    cnt = 10;
    while(cnt--)
    {
        err = uart_write(fd,w_buf,w_len);
        if(err == -1)
        {
            fprintf(stderr,"uart write failed!\n");
            exit(EXIT_FAILURE);
        }
    }
    //recv test
    while(1)
    {
        sleep(1);
	  //rcv ok
        rcv_len =  uart_read(fd, rcv_buf, sizeof(rcv_buf));
        if(rcv_len != -1 && rcv_len != 0)
        {          
            uart_write(fd,rcv_buf,rcv_len);
            //decode         
        }
    }
    close(fd);
    exit(EXIT_SUCCESS);
    
    erro:
    return 1;   
    
}
/**************************************************************************
  * @brief        : linux环境下 串口常用接口
  * @author       : shiweisun@foxmail.com
  * @copyright    : NONE
  * @version      : 0.1
  * @note         :   
  * @history      : 
***************************************************************************/
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include<assert.h>
#include<termios.h>
#include<string.h>
#include<sys/time.h>
#include<sys/types.h>
#include<errno.h>
#include "my_typedef.h"
#include "uart_ctrl.h"

static void set_baudrate (struct termios *opt, unsigned int baudrate)
{
	cfsetispeed(opt, baudrate);
	cfsetospeed(opt, baudrate);
}
static void set_data_bit (struct termios *opt, unsigned int databit)
{
    opt->c_cflag &= ~CSIZE;
    switch (databit) {
    case 8:
        opt->c_cflag |= CS8;
        break;
    case 7:
        opt->c_cflag |= CS7;
        break;
    case 6:
        opt->c_cflag |= CS6;
        break;
    case 5:
        opt->c_cflag |= CS5;
        break;
    default:
        opt->c_cflag |= CS8;
        break;
    }
}
 static void set_parity (struct termios *opt, char parity)
 {
     switch (parity) {
     case 'N':                  /* no parity check */
         opt->c_cflag &= ~PARENB;
         break;
     case 'E':                  /* even */
         opt->c_cflag |= PARENB;
         opt->c_cflag &= ~PARODD;
         break;
     case 'O':                  /* odd */
         opt->c_cflag |= PARENB;
         opt->c_cflag |= ~PARODD;
         break;
     default:                   /* no parity check */
         opt->c_cflag &= ~PARENB;
         break;
     }
 }
 static void set_stopbit (struct termios *opt, const char *stopbit)
 {
     if (0 == strcmp (stopbit, "1")) {
         opt->c_cflag &= ~CSTOPB; /* 1 stop bit */
     }   else if (0 == strcmp (stopbit, "1")) {
         opt->c_cflag &= ~CSTOPB; /* 1.5 stop bit */
     }   else if (0 == strcmp (stopbit, "2")) {
         opt->c_cflag |= CSTOPB;  /* 2 stop bits */
     } else {
         opt->c_cflag &= ~CSTOPB; /* 1 stop bit */
     }
 }


 //串口设置
 int  uart_set_port_attr (
               int fd,
               int  baudrate,          // B1200 B2400 B4800 B9600 .. B115200
               int  databit,           // 5, 6, 7, 8
               const char *stopbit,    //  "1", "1.5", "2"
               char parity,            // N(o), O(dd), E(ven)
               int vtime,
               int vmin )
 {
      struct termios opt;
      tcgetattr(fd, &opt);
      opt.c_iflag = 0;
      opt.c_oflag         = 0;
      //设置波特率
      set_baudrate(&opt, baudrate);
      opt.c_cflag         |= CLOCAL | CREAD;      /* | CRTSCTS */
      //设置数据位
      set_data_bit(&opt, databit);
      //设置校验位
      set_parity(&opt, parity);
      //设置停止位
      set_stopbit(&opt, stopbit);
      //其它设置
      
      opt.c_lflag         = 0;
      opt.c_iflag &= ~(BRKINT | ICRNL | INPCK | ISTRIP | IXON);     	//关闭特殊字符
    // opt.c_lflag &= ~(ICANON | ECHO | ECHOE | ECHOK | ECHONL | NOFLSH);//关闭回显
      opt.c_oflag            &= ~OPOST;
    opt.c_cc[VINTR]    = 0;     /* Ctrl-c */ 
    opt.c_cc[VQUIT]    = 0;     /* Ctrl-/ */
    opt.c_cc[VERASE]   = 0;     /* del */
    opt.c_cc[VKILL]    = 0;     /* @ */
    opt.c_cc[VEOF]     = 0;     /* Ctrl-d */
    opt.c_cc[VTIME]    = 0;     /* 用分割字元组的计时器 */
    opt.c_cc[VMIN]     = 0;     /* 在读取到 1 个字元前先停止 */
    opt.c_cc[VSWTC]    = 0;     /* '/0' */
    opt.c_cc[VSTART]   = 0;     /* Ctrl-q */ 
    opt.c_cc[VSTOP]    = 0;     /* Ctrl-s */
    opt.c_cc[VSUSP]    = 0;     /* Ctrl-z */
    opt.c_cc[VEOL]     = 0;     /* '/0' */
    opt.c_cc[VREPRINT] = 0;     /* Ctrl-r */
    opt.c_cc[VDISCARD] = 0;     /* Ctrl-u */
    opt.c_cc[VWERASE]  = 0;     /* Ctrl-w */
    opt.c_cc[VLNEXT]   = 0;     /* Ctrl-v */
    opt.c_cc[VEOL2]    = 0;     /* '/0' */
      opt.c_cc[VTIME]         = vtime;
      opt.c_cc[VMIN]              = vmin;
      tcflush (fd, TCIFLUSH);
      return (tcsetattr (fd, TCSANOW, &opt));
 }



int init_dev_rcv_cmd(int *fd)
{    
    //*fd = open(DEVPATH_RCV_CMD, O_RDWR|O_NOCTTY|O_SYNC);//阻塞模式
    *fd = open(DEVPATH_RCV_CMD, O_RDWR|O_NOCTTY);//阻塞模式
    if(*fd < 0) 
    {
       perror(DEVPATH_RCV_CMD);
       return -1;
    }
    uart_set_port_attr(*fd, B115200, 8, "1",'N', 0,0 );//init uart
    return 0;
}


/** 
*串口发送数据
*@fd:串口描述符
*@data:待发送数据
*@datalen:数据长度
*/  
int uart_write(int fd, void *w_buf, int datalen)
{  
    int len = 0;
    
    len = write(fd, w_buf, datalen);//实际写入的长度
    if (len == datalen) 
    {
        return len;
    } else 
    {
        return -1;
    }    
    return 0;
}  
  
/**
*
*
*/  
int uart_read(int fd, void *r_buf, int datalen)  
{
    int len = 0;
    fd_set fs_read;
    struct timeval tv_timeout;    
    FD_ZERO(&fs_read);
    FD_SET(fd, &fs_read);
    tv_timeout.tv_sec  = 1;
    tv_timeout.tv_usec = 0;
    
	select(fd+1, &fs_read, NULL, NULL, &tv_timeout);
    
    if (FD_ISSET(fd, &fs_read)) 
    {
        len = read(fd, r_buf, datalen);
        return len;
    } 
    else 
    {      
        return -1;
    }    
    return 0;
}




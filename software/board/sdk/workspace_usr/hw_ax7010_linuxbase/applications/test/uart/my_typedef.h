/**********************************************************************************************
 *@brief file name:typedef_my.h
 *@function 变量别名定义
***********************************************************************************************/
#ifndef __MY_TYPEDEF_H__
#define __MY_TYPEDEF_H__


#include "stdlib.h"
typedef unsigned char               u8;
typedef unsigned short int         u16;
typedef unsigned int          u32;
typedef unsigned long long int  u64;

typedef signed char               s8;
typedef signed short int         s16;
typedef signed int          s32;
typedef signed long long int  s64;

typedef volatile unsigned char               vu8;
typedef volatile unsigned short int         vu16;
typedef volatile unsigned int          vu32;
typedef volatile unsigned long long int  vu64;

typedef volatile signed char                    vs8;
typedef volatile signed short int             vs16;
typedef volatile signed int              vs32;
typedef volatile signed long long int      vs64;


#ifndef FALSE
#define FALSE    (0)
#endif

#ifndef fasle
#define fasle    (0)
#endif

#ifndef TRUE
#define TRUE  (!FALSE)
#endif

#ifndef true
#define true (!false)
#endif

#ifndef DIV_ROUND
#define DIV_ROUND(divident, divider)    ( ( (divident)+((divider)>>1)) / (divider) )
#endif

#ifndef ROUND_UP
#define ROUND_UP(x, n)  ( ((x)+(n)-1u) & ~((n)-1u) )
#endif

#ifndef MIN
#define MIN(a, b) ((a) > (b) ? (b) : (a))
#endif

#ifndef MAX
#define MAX(a, b) ((a) > (b) ? (a) : (b))
#endif

#ifndef ABS
#define ABS(x) ({               \
        int __x = (x);          \
        (__x < 0) ? -__x : __x;     \
            })
#endif

#ifndef ARRAY_SIZE
#define ARRAY_SIZE(array)   (sizeof(array) / sizeof(array[0]))
#endif

#endif  /*__MY_TYPEDEF__*/


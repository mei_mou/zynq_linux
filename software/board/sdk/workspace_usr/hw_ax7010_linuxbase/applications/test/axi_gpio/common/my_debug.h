/**********************************************************************************************
 *@brief file name:me_debug.h
 *@function 调试打印控制
***********************************************************************************************/
#ifndef __MY_DEBUG_H__
#define __MY_DEBUG_H__
#include <unistd.h>
#include <stdlib.h>


#define GREEN_START     "\033[32m"
#define YELLOW_START    "\033[33m"
#define RED_START       "\033[31m"
#define COLOR_NONE             "\033[0m"

#ifndef DEBUG_PRINT_EN
#define DEBUG_PRINT_EN (0)
#endif

#if DEBUG_PRINT_EN
#define PRINT_INFO(args...)     {printf(YELLOW_START);printf(args);printf(COLOR_NONE);}
#define PRINT_RED(args...)       {printf(RED_START);printf(args);printf(COLOR_NONE);}
#define PRINT_GREEN(args...)     {printf(GREEN_START);printf(args);printf(COLOR_NONE);}
#define PRINT_DEBUG(args...)      printf(args)
#define PRINT_FUNC_NAME             PRINT_INFO("FUN:[%s],\nfile:%s,Line:%d\n",__FUNCTION__,__FILE__,__LINE__)
#else
#define PRINT_INFO(args...)
#define PRINT_RED(args...)
#define PRINT_GREEN(args...)
#define PRINT_DEBUG(args...)
#define PRINT_FUNC_NAME
#endif
#define PRINT_ERROR  PRINT_RED
#define PRINT_WARM  PRINT_INFO

#endif  /*__MY_DEBUG_H__*/


/*!
*****************************************************************************
** \file        adi/include/adi_sys.h
**
** \brief       ADI system functionality module header file.
**
** \attention   THIS SAMPLE CODE IS PROVIDED AS IS. xxx MICROELECTRONICS
**              ACCEPTS NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR
**              OMMISSIONS
**
** (C) Copyright 2015-2019 by xxx MICROELECTRONICS CO.,LTD
**
*****************************************************************************
*/
#ifndef _ADI_SYS_H_
#define _ADI_SYS_H_

#include "stdio.h"
#include "adi_types.h"
#include "my_typedef.h"

//*****************************************************************************
//*****************************************************************************
//** Defines and Macros
//*****************************************************************************
//*****************************************************************************

#define Q9_BASE         (512000000)




/*
*******************************************************************************
** Defines for general error codes of the module.
*******************************************************************************
*/
/*! Bad parameter passed. */
#define ADI_SYS_ERR_BAD_PARAMETER                                          \
                              (ADI_SYS_MODULE_BASE + ADI_ERR_BAD_PARAMETER)
/*! Memory allocation failed. */
#define ADI_SYS_ERR_OUT_OF_MEMORY                                          \
                              (ADI_SYS_MODULE_BASE + ADI_ERR_OUT_OF_MEMORY)
/*! Device already initialised. */
#define ADI_SYS_ERR_ALREADY_INITIALIZED                                    \
                        (ADI_SYS_MODULE_BASE + ADI_ERR_ALREADY_INITIALIZED)
/*! Device not initialised. */
#define ADI_SYS_ERR_NOT_INITIALIZED                                        \
                            (ADI_SYS_MODULE_BASE + ADI_ERR_NOT_INITIALIZED)
/*! Feature or function is not available. */
#define ADI_SYS_ERR_FEATURE_NOT_SUPPORTED                                  \
                      (ADI_SYS_MODULE_BASE + ADI_ERR_FEATURE_NOT_SUPPORTED)
/*! Timeout occured. */
#define ADI_SYS_ERR_TIMEOUT                                                \
                                    (ADI_SYS_MODULE_BASE + ADI_ERR_TIMEOUT)
/*! The device is busy, try again later. */
#define ADI_SYS_ERR_DEVICE_BUSY                                            \
                                (ADI_SYS_MODULE_BASE + ADI_ERR_DEVICE_BUSY)
/*! Invalid handle was passed. */
#define ADI_SYS_ERR_INVALID_HANDLE                                         \
                             (ADI_SYS_MODULE_BASE + ADI_ERR_INVALID_HANDLE)
/*! Semaphore could not be created. */
#define ADI_SYS_ERR_SEMAPHORE_CREATE                                       \
                           (ADI_SYS_MODULE_BASE + ADI_ERR_SEMAPHORE_CREATE)
/*! The driver's used version is not supported. */
#define ADI_SYS_ERR_UNSUPPORTED_VERSION                                    \
                        (ADI_SYS_MODULE_BASE + ADI_ERR_UNSUPPORTED_VERSION)
/*! Thread could not be created. */
#define ADI_SYS_ERR_THREAD_CREATE                                          \
                        (ADI_SYS_MODULE_BASE + ADI_ERR_THREAD_CREATE)
/*
*******************************************************************************
** Defines for specialized error codes which are very specific
** to the behaviour of this module. These codes are offset by -100 from
** the module general error codes.
*******************************************************************************
*/
/*!
*******************************************************************************
** \brief Error base for system module local errors.
*******************************************************************************
*/
#define GAPI_SYS_ERR_BASE                      (ADI_SYS_MODULE_BASE - 100)
/*!
*******************************************************************************
** \brief Seamphore handle is invalid.
*******************************************************************************
*/
#define ADI_SYS_ERR_INVALID_SEMAPHORE_HANDLE                                 \
                                                      (GAPI_SYS_ERR_BASE -   1)
/*!
*******************************************************************************
** \brief Message queue creating fail.
*******************************************************************************
*/
#define ADI_SYS_ERR_MSG_QUEUE_CREATE                                         \
                                                      (GAPI_SYS_ERR_BASE -   2)
/*!
*******************************************************************************
** \brief Message queue full.
*******************************************************************************
*/
#define ADI_SYS_ERR_MSG_QUEUE_FULL                                           \
                                                      (GAPI_SYS_ERR_BASE -   3)
/*!
*******************************************************************************
** \brief Message queue error happened in sending process.
*******************************************************************************
*/
#define ADI_SYS_ERR_MSG_QUEUE_SEND                                           \
                                                      (GAPI_SYS_ERR_BASE -   4)
#define ADI_SYS_THREAD_PRIO_MIN                      0
#define ADI_SYS_THREAD_PRIO_MAX                      99
#define ADI_SYS_THREAD_PRIO_DEFAULT                  ADI_SYS_THREAD_PRIO_MIN
#define ADI_SYS_THREAD_STATCK_SIZE_DEFAULT           4096 /* Kbyte*/
/*!
*******************************************************************************
** \brief thread function prototype
*******************************************************************************
*/
typedef ADI_VOID (*ADI_SYS_ThreadFunctionT)(ADI_VOID *);


//*****************************************************************************
//*****************************************************************************
//** Enumerated types
//*****************************************************************************
//*****************************************************************************

/*!
*******************************************************************************
** \brief adi log level.
*******************************************************************************
*/
typedef enum {
    ADI_SYS_LOG_LEVEL_ERROR = 0,
    ADI_SYS_LOG_LEVEL_DEBUG,
    ADI_SYS_LOG_LEVEL_INFO,
    ADI_SYS_LOG_LEVEL_NUM,
} ADI_SYS_LogLevelEnumT;


//*****************************************************************************
//*****************************************************************************
//** API Functions
//*****************************************************************************
//*****************************************************************************

#ifdef __cplusplus
extern "C" {
#endif

/*!
*******************************************************************************
** \brief Initialize the ADI system module.
**
**
** \return
** - #ADI_OK
** - #ADI_ERR_ALREADY_INITIALIZED
**
** \sa adi_sys_exit
**
*******************************************************************************
*/
ADI_ERR adi_sys_init(void);

/*!
*******************************************************************************
** \brief Shutdown the ADI system module.
**
** \return
** - #ADI_OK
** - #ADI_ERR_NOT_INITIALIZED
**
** \sa adi_sys_init
**
*******************************************************************************
*/
ADI_ERR adi_sys_exit(void);

/*!
*******************************************************************************
** \brief Get the ADI layer printf log level.
**
** \param[out] plogLevel pointer to get the log level.
**
** \return
** - #ADI_OK
** - #ADI_SYS_ERR_BAD_PARAMETER
**
** \sa adi_sys_set_log_level
**
*******************************************************************************
*/
ADI_ERR adi_sys_get_log_level(ADI_U32* plogLevel);

/*!
*******************************************************************************
** \brief Set the ADI layer printf log level.
**
** \param[in] logLevel Specifies the print log level.
**
** \return
** - #ADI_OK
** - #ADI_SYS_ERR_BAD_PARAMETER
**
** \sa adi_sys_get_log_level
**
*******************************************************************************
*/
ADI_ERR adi_sys_set_log_level(ADI_SYS_LogLevelEnumT logLevel);

/*!
*******************************************************************************
** \brief Allocates \e size bytes and returns the pointer to the allocated memory.
**
** \param[in]  size   Memory size in bytes.
**
** \return In success, return the valid memory pointer.
**         In failure, return NULL.
**
** \sa adi_sys_free
**
*******************************************************************************
*/
ADI_VOID *adi_sys_malloc(ADI_U32 size);

/*!
*******************************************************************************
** \brief Free the memory space pointed to by \e prt, which must have been
**        returned by a previous call to #adi_sys_malloc.
**
** \note  If ptr is NULL, no operation will be performed.
**
** \param[in]  ptr   Pointer to the memory space allocated previously by
**                   #adi_sys_malloc.
**
**
** \sa
**  - adi_sys_malloc
**
*******************************************************************************
*/
ADI_VOID adi_sys_free(ADI_VOID *ptr);

/*!
*******************************************************************************
** \brief This API copies \e size bytes from source memory \e src to the
**        destnation memory \e dest.
**
** \note  The \e dest and \e src should not overlap.
**
**
** \param[in]  dest  Destnation memory.
** \param[in]  src   Source memory.
** \param[in]  size  Size in bytes.
**
** \return
**      - #NULL memory copy fail.
**      - >0    a pointer to the destination address.
**
** \sa adi_sys_memset
**
*******************************************************************************
*/
ADI_VOID* adi_sys_memcpy(ADI_VOID *dest, ADI_VOID *src, ADI_U32 size);

/*!
*******************************************************************************
** \brief This API fills the first \e n bytes of the memory area pointed to by
**        the pointer of \e ptr, with the constant byte \e c.
**
**
** \param[in]  ptr  Pointer to the memory area to be filled.
** \param[in]  c    Constant byte value used to fill the specified memory.
** \param[in]  n    Number of bytes to fill.
**
** \return
**      - #NULL memory set fail.
**      - >0    a pointer to the destination address.
**
** \sa adi_sys_memcpy
**
*******************************************************************************
*/
ADI_VOID* adi_sys_memset(ADI_VOID *ptr, ADI_U8 c, ADI_U32 n);

/*!
*******************************************************************************
** \brief Create an semphore with the given initial value \e initValue.
**
**
** \param[in] initValue Specifies the initial value for the semaphore.
**
**
** \return 
**      - #Return an valid handle value if the semaphore creating is successful.
**      - #0 if creating failed.
**
** \sa adi_sys_sem_destroy
**
*******************************************************************************
*/
ADI_SYS_SemHandleT adi_sys_sem_create(ADI_U32 initValue);

/*!
*******************************************************************************
** \brief Increments(unlocks) the semaphore referred by \e sem.
**
**
** \param[in] sem   Valid semaphore handle created previously by
**                  #adi_sys_sem_create.
**
**
** \return
** - #ADI_OK   Successful completion.
** - #ADI_SYS_ERR_INVALID_SEMAPHORE_HANDLE
**
** \sa adi_sys_sem_wait
** \sa adi_sys_sem_wait_timeout
**
*******************************************************************************
*/
ADI_ERR adi_sys_sem_post(ADI_SYS_SemHandleT sem);

/*!
*******************************************************************************
** \brief Dncrements(locks) the semaphore referred by \e sem.
**
**
** \param[in] sem   Valid semaphore handle created previously by
**                  #adi_sys_sem_create.
**
**
** \return
** - #ADI_OK   Successful completion.
** - #ADI_SYS_ERR_INVALID_SEMAPHORE_HANDLE
**
** \sa
**  - adi_sys_sem_post
**  - adi_sys_sem_wait_timeout
**
*******************************************************************************
*/
ADI_ERR adi_sys_sem_wait(ADI_SYS_SemHandleT sem);


/*!
*******************************************************************************
** \brief This API is the same as adi_sys_sem_wait, except that \e msecs
**        specifies a limitation on the amount of the time that the call should
**        block if the decrement can not be performed immediately.
**
**
** \param[in] sem   Valid semaphore handle created previously by
**                  #adi_sys_sem_create.
** \param[in] msecs Timeout value in millisecond.
**
**
** \return
** - #ADI_OK   Successful completion.
** - #ADI_SYS_ERR_TIMEOUT
**
** \sa
**      - adi_sys_sem_post
**      - adi_sys_sem_wait
**
*******************************************************************************
*/
ADI_ERR adi_sys_sem_wait_timeout(ADI_SYS_SemHandleT sem, ADI_U32 msecs);

/*!
*******************************************************************************
** \brief Destroy the semaphore \e sem.
**
**
** \param[in] sem   Valid semaphore handle created previously by
**                  #adi_sys_sem_create.
**
**
** \return
** - #ADI_OK   Successful completion.
** - #ADI_SYS_ERR_INVALID_SEMAPHORE_HANDLE
**
** \sa adi_sys_sem_create
**
*******************************************************************************
*/
ADI_ERR adi_sys_sem_destroy(ADI_SYS_SemHandleT sem);

/*!
*******************************************************************************
** \brief Starts a new thread in the call process.
**
**
** \param[in]  func     The routine executed in the created thread.
** \param[in]  arg      The argument passed to the \e func.
** \param[in]  priority Priority setting.
** \param[in]  ssize    Stack size in Kbytes.
**                      In RTOS environment, this parameter is ignored.
** \param[in]  name     Thread name.
** \param[out] thread   Thread identifier or handle.
**
** \return
** - #ADI_OK
** - #ADI_SYS_ERR_OUT_OF_MEMORY
** - #ADI_SYS_ERR_THREAD_CREATE
**
** \sa
**      - adi_sys_thread_destroy
**
*******************************************************************************
*/
ADI_ERR adi_sys_thread_create(ADI_SYS_ThreadFunctionT func,
                            ADI_VOID              *arg,
                            ADI_S32                priority,
                            ADI_S32                ssize,
                            const ADI_CHAR          *name,
                            ADI_SYS_ThreadHandleT *thread);

/*!
*******************************************************************************
** \brief Start multitasking.
**
** \note  This API is valid only in RTOS environment. In Linux, this API does
**        nothing.
**
*******************************************************************************
*/
ADI_VOID adi_sys_multi_task_start(void);

/*!
*******************************************************************************
** \brief Stop and destroy the given thread.
**
**
** \param[in]  thread The identifier of the thread to be destroyed.
**
** \return
** - #ADI_OK
** - #ADI_SYS_ERR_INVALID_HANDLE
** - #ADI_SYS_ERR_BAD_PARAMETER
**
** \sa
**      - adi_sys_thread_create
**
*******************************************************************************
*/
ADI_ERR adi_sys_thread_destroy(ADI_SYS_ThreadHandleT thread);

/*!
*******************************************************************************
** \brief Stop and destroy the calling thread.
**
**
** \sa
**      - adi_sys_thread_self
**
*******************************************************************************
*/
ADI_SYS_ThreadHandleT adi_sys_thread_self(void);

/*!
*******************************************************************************
** \brief Stop and destroy the calling thread.
**
**
** \sa
**      - adi_sys_thread_destroy
**
*******************************************************************************
*/
ADI_VOID adi_sys_thread_self_destroy(void);

/*!
*******************************************************************************
** \brief Makes the call thread to sleep until the given milliseconds have passed.
**
**
** \param[in] msecs Sleep time in milliseconds.
**
**
*******************************************************************************
*/
ADI_VOID adi_sys_thread_sleep(ADI_U32 msecs);

/*!
*******************************************************************************
** \brief causes the calling thread to relinquish the CPU.
**
** \return
**      - #ADI_OK
**
*******************************************************************************
*/
ADI_ERR adi_sys_thread_yield(void);

/*!
*******************************************************************************
** \brief Suspend the specified thread.
**
**
** \param[in]  thread The identifier of the thread to be suspended.
**
**
** \sa
**      - adi_sys_thread_resume
**
*******************************************************************************
*/
ADI_VOID adi_sys_thread_suspend(ADI_SYS_ThreadHandleT thread);

/*!
*******************************************************************************
** \brief Suspend the thread which is call this API.
**
**
** \sa
**      - adi_sys_thread_resume
**
*******************************************************************************
*/
ADI_VOID adi_sys_thread_self_suspend(void);

/*!
*******************************************************************************
** \brief Resume the specified thread which has been suspended before.
**
**
** \param[in]  thread The identifier of the thread to be resumed.
**
**
** \sa adi_sys_thread_suspend
**
*******************************************************************************
*/
ADI_VOID adi_sys_thread_resume(ADI_SYS_ThreadHandleT thread);

/*!
*******************************************************************************
** \brief Wait for termination of another thread.
**
** this function suspends execution of the calling thread until the target
** thread terminates, unless the target thread has already terminated.
**
** \param[in]  thread The identifier of the thread to be waited.
**
** \return
**      - #ADI_OK
**      - #ADI_ERR_INVALID_HANDLE
**
** \sa adi_sys_thread_create
**
*******************************************************************************
*/
ADI_ERR adi_sys_wait_end_thread(ADI_SYS_ThreadHandleT thread);

/*!
*******************************************************************************
** \brief Display the thread statistics information via uart message.
*******************************************************************************
*/
ADI_VOID adi_sys_thread_statistics(void);


/*!
*******************************************************************************
** \brief Get the current date info.
**
** \str[out] out put the date information as string format.
**
**
*******************************************************************************
*/
ADI_VOID adi_sys_get_date(ADI_CHAR *str);


/*!
*******************************************************************************
** \brief Print a debug message in debug mode.
**
** \param format The aruments in 'printf' style.
**
*******************************************************************************
*/
#ifdef DEBUG /* DEBUG is only valid in the debug-mode make process */

extern ADI_U32 adiLogLevel;

#define ADI_PRINT(mylog, LOG_LEVEL, format, args...)    \
                do {                                    \
                    if (mylog >= LOG_LEVEL) {           \
                        printf(format, ##args);         \
                    }                                   \
                } while (0)

#define ADI_ERROR(format, args...) ADI_PRINT(adiLogLevel, ADI_SYS_LOG_LEVEL_ERROR, "[ERROR] [%s: %d]" format "\n", __FILE__, __LINE__, ##args)
#define ADI_DEBUG(format, args...) ADI_PRINT(adiLogLevel, ADI_SYS_LOG_LEVEL_DEBUG, "[DEBUG] " format, ##args)
#define ADI_INFO(format, args...)  ADI_PRINT(adiLogLevel, ADI_SYS_LOG_LEVEL_INFO,  "[INFO] " format, ##args)
#define ADI_PRI(format, args...)   printf("[PRI] " format, ##args)

#else

#define ADI_ERROR(format, args...) printf("[ERROR] " format, ##args)
#define ADI_DEBUG(format, args...)
#define ADI_INFO(format, args...)
#define ADI_PRI(format, args...)   printf("[PRI] " format, ##args)

#endif

#ifdef __cplusplus
    }
#endif


#endif /* _ADI_SYS_H_ */

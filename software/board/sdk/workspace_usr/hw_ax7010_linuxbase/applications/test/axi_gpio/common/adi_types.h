/*!
*****************************************************************************
** \file        ./adi/inc/adi_types.h
**
** \brief       adapte driver interface type define
**
** \attention   THIS SAMPLE CODE IS PROVIDED AS IS. ---- MICROELECTRONICS
**              ACCEPTS NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR
**              OMMISSIONS
**
** (C) Copyright 2012-2013 by ---- MICROELECTRONICS CO.,LTD
**
*****************************************************************************
*/

#ifndef _ADI_TYPES_H_
#define _ADI_TYPES_H_

#include "adi_sys_error.h"
#include "basetypes.h"

//*****************************************************************************
//*****************************************************************************
//** Defines and Macros
//*****************************************************************************
//*****************************************************************************
/*!
*******************************************************************************
** \brief ADI type define
*******************************************************************************
*/
typedef unsigned char       ADI_U8;         /*!< 8 bit unsigned integer. */
typedef unsigned short      ADI_U16;        /*!< 16 bit unsigned integer. */
typedef unsigned int        ADI_U32;        /*!< 32 bit unsigned integer. */
typedef unsigned long long  ADI_U64;        /*!< 64 bit unsigned integer. */
typedef signed char         ADI_S8;         /*!< 8 bit signed integer. */
typedef signed short        ADI_S16;        /*!< 16 bit signed integer. */
typedef signed int          ADI_S32;        /*!< 32 bit signed integer. */
typedef signed long long    ADI_S64;        /*!< 64 bit unsigned integer. */
typedef signed int          ADI_ERR;        /*!< error code type .*/
typedef unsigned int        ADI_HANDLE;     /*!< 32 bit unsigned integer. */
typedef unsigned short      ADI_USHORT;     /*!< unsigned short. */
typedef short               ADI_SHORT;      /*!< short. */
typedef unsigned int        ADI_UINT;       /*!< unsigned int. */
typedef int                 ADI_INT;        /*!< int. */
typedef unsigned long       ADI_ULONG;      /*!< unsigned long. */
typedef long                ADI_LONG;       /*!< long. */
typedef unsigned long long  ADI_ULONGLONG; /*!< unsigned long long. */
typedef long long           ADI_LONGLONG;  /*!< long long. */
typedef char                ADI_UCHAR;      /*!< unsigned char */
typedef char                ADI_CHAR;       /*!< char */
typedef void                ADI_VOID;       /*!< void */

/*! common handle type */
typedef void*               ADI_SYS_HandleT;

/*! semaphore handle type */
typedef unsigned int        ADI_SYS_SemHandleT;

/*! thread handle type */
typedef unsigned int        ADI_SYS_ThreadHandleT;


#if defined(__HIGHC__)
    #undef NULL
    #define NULL 0
#else

#ifndef NULL
    #define NULL 0  /*!< NULL define to 0. */
#endif
#endif


/*!
*******************************************************************************
** \brief Common inline key word.
*******************************************************************************
*/
#if defined(__HIGHC__)
#define FINLINE _Inline
#else
#define FINLINE inline
#endif

#if defined(__GNUC__)
#define _Unaligned
#endif

/*!
*******************************************************************************
** \brief Branch optimize macro.
*******************************************************************************
*/
#ifdef __GNUC__
#define LIKELY(x)       __builtin_expect(!!(x), 1)
#define UNLIKELY(x)     __builtin_expect(!!(x), 0)
#else
#define LIKELY(x)       (x)
#define UNLIKELY(x)     (x)
#endif

//*****************************************************************************
//*****************************************************************************
//** Enumerated types
//*****************************************************************************
//*****************************************************************************
/*!
*******************************************************************************
** \brief ADI BOOL enum
*******************************************************************************
*/
typedef enum {
    ADI_FALSE = 0,  /*!< Logical false. */
    ADI_TRUE  = 1   /*!< Logical true. */
} ADI_BOOL;


//*****************************************************************************
//*****************************************************************************
//** Data Structures
//*****************************************************************************
//*****************************************************************************



//*****************************************************************************
//*****************************************************************************
//** Global Data
//*****************************************************************************
//*****************************************************************************



//*****************************************************************************
//*****************************************************************************
//** API Functions
//*****************************************************************************
//*****************************************************************************



#ifdef __cplusplus
extern "C" {
#endif



#ifdef __cplusplus
}
#endif


#endif /* _ADI_TYPES_H_ */


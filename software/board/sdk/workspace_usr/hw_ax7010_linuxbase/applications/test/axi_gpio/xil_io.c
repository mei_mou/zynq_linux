#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/mman.h>
 
#include <unistd.h>
#include <fcntl.h>
#include "xil_io.h"
#define PAGE_SIZE  ((size_t)getpagesize())
#define PAGE_MASK ((uint32_t) (long)~(PAGE_SIZE - 1))

#define GREEN_START     "\033[32m"
#define YELLOW_START    "\033[33m"
#define RED_START       "\033[31m"
#define COLOR_NONE             "\033[0m"

#define DEBUG_PRINT_EN (1)
#ifndef DEBUG_PRINT_EN
#define DEBUG_PRINT_EN (0)
#endif

#if DEBUG_PRINT_EN
#define PRINT_INFO(args...)     {printf(YELLOW_START);printf(args);printf(COLOR_NONE);}
#define PRINT_RED(args...)       {printf(RED_START);printf(args);printf(COLOR_NONE);}
#define PRINT_GREEN(args...)     {printf(GREEN_START);printf(args);printf(COLOR_NONE);}
#define PRINT_DEBUG(args...)      printf(args)
#define PRINT_FUNC_NAME             PRINT_INFO("FUN:[%s],\nfile:%s,Line:%d\n",__FUNCTION__,__FILE__,__LINE__)
#else
#define PRINT_INFO(args...)
#define PRINT_RED(args...)
#define PRINT_GREEN(args...)
#define PRINT_DEBUG(args...)
#define PRINT_FUNC_NAME
#endif
#define PRINT_ERROR  PRINT_RED
#define PRINT_WARM  PRINT_INFO
struct xil_mem_s g_xil_mem;

int xil_dev_mem_init()
{
    
    struct xil_mem_s *xil_mem = &g_xil_mem;
  
PRINT_FUNC_NAME;
    /*
    0x7C40_0000  ~ 0x7C40_FFFF  ;  
    0x5000_0000 ~ 0x5002_FFFF ; 
    0x5100_0000 ~0x5100_FFFF
    */
	uint32_t base7c = 0x7C400000;
    uint32_t base50 = 0x50000000;
    uint32_t base51 = 0x51000000;
	//open /dev/mem
	printf("getpagesize:%#X\n",(size_t)getpagesize());
 	xil_mem->fd = NULL;
	if((xil_mem->fd = open("/dev/mem", O_RDWR | O_SYNC)) == -1)
	{
		perror("open /dev/mem: erro!\n");
        goto err;
	}
	PRINT_DEBUG("step1\n");
	//mmap
	xil_mem->map_base7c = mmap(NULL, 0x10000, PROT_READ | PROT_WRITE, MAP_SHARED,
			xil_mem->fd , base7c);
    
	if(xil_mem->map_base7c == MAP_FAILED)
	{
		perror("mmap: erro!\n");
        
        goto err;
	}
	PRINT_DEBUG("step2\n");
    xil_mem->map_base50 = mmap(NULL, 0x30000, PROT_READ | PROT_WRITE, MAP_SHARED,
                xil_mem->fd , base50);
        
    if(xil_mem->map_base50 == MAP_FAILED)
    {
        perror("mmap: erro!\n");
        
        goto err;
    }
    xil_mem->map_base51 = mmap(NULL, 0x10000, PROT_READ | PROT_WRITE, MAP_SHARED,
		xil_mem->fd , base51);
    
	if(xil_mem->map_base51 == MAP_FAILED)
	{
		perror("mmap: erro!\n");
       
        goto err;
	}

    PRINT_DEBUG("step end\n");
    return 0;
    err:
        xil_dev_mem_deinit();
    return 1;
}
int xil_dev_mem_deinit(void)
{
    struct xil_mem_s *xil_mem = &g_xil_mem;
   
	printf("%s\n",__func__);
    if(NULL != xil_mem->fd)
    {
        if(NULL != xil_mem->map_base7c)
        {
            munmap((void *)xil_mem->map_base7c, PAGE_SIZE);
            xil_mem->map_base7c = NULL;
        }
        if(NULL != xil_mem->map_base50)
        {
            munmap((void *)xil_mem->map_base50, PAGE_SIZE);
            xil_mem->map_base50 = NULL;
        }
        
        if(NULL != xil_mem->map_base51)
        {
            munmap((void *)xil_mem->map_base51, PAGE_SIZE);
            xil_mem->map_base51 = NULL;
        }
        close(xil_mem->fd);
    }
    return 0;
}
#if 1
#if 0
 void Xil_Out32(uint32_t phyaddr, uint32_t val)
{
	volatile uint32_t pgoffset;
	volatile uint32_t pagesize;
	uint32_t val_rd;
    struct xil_mem_s *xil_mem = &g_xil_mem;
    /*
        0x7C40_0000  ~ 0x7C40_FFFF  ;  
        0x5000_0000 ~ 0x5002_FFFF ; 
        0x5100_0000 ~0x5100_FFFF
        */      
	PRINT_DEBUG("%s phyaddr:%#X val:%#X\n",__func__,phyaddr,val);
    if(phyaddr >= 0x50000000 && phyaddr < 0x5002FFFF )
    {
        pagesize = 0x30000;
        pgoffset = phyaddr &  (pagesize - 1);
        *(volatile uint32_t *)(xil_mem->map_base50 + pgoffset) = val;
    }


    else if(phyaddr >= 0x51000000  && phyaddr < 0x5100FFFF )
    {
        pagesize = 0x10000;
        pgoffset = phyaddr &  (pagesize - 1);
        *(volatile uint32_t *)(xil_mem->map_base51 + pgoffset) = val;
    }

    
	else if(phyaddr >= 0x7C400000 && phyaddr < 0x7C40FFFF )
    {
        pagesize = 0x10000;
        pgoffset = phyaddr &  (pagesize - 1);
	//	PRINT_DEBUG("%s map_base7c:%#X pgoffset:%#X\n",__func__,xil_mem->map_base7c,pgoffset);
        *(volatile uint32_t *)(xil_mem->map_base7c + pgoffset) = val;
		
    }
	usleep(100000);
}

 
uint32_t Xil_In32(uint32_t phyaddr)
{
    volatile uint32_t val;
	volatile uint32_t pgoffset,pagesize ;
    struct xil_mem_s *xil_mem = &g_xil_mem;
    
    if(phyaddr >= 0x50000000 && phyaddr < 0x5002FFFF )
    {
        pagesize = 0x10000;
        pgoffset = phyaddr &  (pagesize - 1);
        val = *(volatile uint32_t *)(xil_mem->map_base50 + pgoffset);
    }


    else if(phyaddr >= 0x51000000  && phyaddr < 0x5100FFFF )
    {
        pagesize = 0x10000;
        pgoffset = phyaddr &  (pagesize - 1);
        val = *(volatile uint32_t *)(xil_mem->map_base51 + pgoffset);
    }

    
	else if(phyaddr >= 0x7C400000 && phyaddr < 0x7C40FFFF )
    {
        pagesize = 0x10000;
        pgoffset = phyaddr &  (pagesize - 1);
        val = *(volatile uint32_t *)(xil_mem->map_base7c + pgoffset);
    }
	PRINT_DEBUG("%s phyaddr:%#X val:%#X\n",__func__,phyaddr,val);
	return val;
}
#else
 
void Xil_Out32(uint32_t phyaddr, uint32_t val)
{
	int fd;
	volatile uint8_t *map_base;
	uint32_t base = phyaddr & PAGE_MASK;
	uint32_t pgoffset = phyaddr & (~PAGE_MASK);
 
	if((fd = open("/dev/mem", O_RDWR| O_SYNC )) == -1)
	{
		perror("open /dev/mem:");
	}
 
	map_base = mmap(NULL, PAGE_SIZE, PROT_READ|PROT_WRITE,MAP_SHARED,
			fd, base);
	if(map_base == MAP_FAILED)
	{
		perror("mmap:");
	}
	*(volatile uint32_t *)(map_base + pgoffset) = val; 
    /*
    if(msync((void *)map_base,PAGE_SIZE,MS_SYNC)==-1)  
    {  
        perror("msync fail:");  
        exit(1);  
    } 
    
    */
    fdatasync(fd);
    usleep(20000);
    munmap((void *)map_base, PAGE_SIZE);usleep(20000);
    
	close(fd);
	
}
 
uint32_t Xil_In32(uint32_t phyaddr)
{
	int fd;
	uint32_t val;
	volatile uint8_t *map_base;
	uint32_t base = phyaddr & PAGE_MASK;
	uint32_t pgoffset = phyaddr & (~PAGE_MASK);
	//open /dev/mem
	if((fd = open("/dev/mem", O_RDWR | O_SYNC )) == -1)
	{
		perror("open /dev/mem:");
	}
	//mmap
	map_base = mmap(NULL, PAGE_SIZE, PROT_READ|PROT_WRITE,MAP_SHARED,
			fd, base);
	if(map_base == MAP_FAILED)
	{
		perror("mmap:");
	}
	val = *(volatile uint32_t *)(map_base + pgoffset);
    /*
    if(msync((void *)map_base,PAGE_SIZE,MS_SYNC)==-1)  
    {  
        perror("msync fail:");  
        exit(1);  
    } 
    */
    fdatasync(fd);
    
    usleep(20000);
    munmap((void *)map_base, PAGE_SIZE);usleep(20000);
   
	close(fd);
	
 
	return val;
}

#endif
#endif
#if 0
static void execute_cmd(const char *cmd, char *result)
{
    char buf_ps[1024];
    char ps[1024]={0};
    FILE *ptr;
    strcpy(ps, cmd);
    if((ptr=popen(ps, "r"))!=NULL)
    {
        while(fgets(buf_ps, 1024, ptr)!=NULL)
        {
           strcat(result, buf_ps);
           if(strlen(result)>1024)
               break;
        }
        pclose(ptr);
        ptr = NULL;
    }
    else
    {
        printf("popen %s error\n", ps);
    }
}


static void memory_set(uint32_t addr , uint32_t len, uint32_t val)
{
	pid_t status;
	uint8_t cmd[256];
	uint8_t result[1024];
	PRINT_FUNC_NAME;
	sprintf(cmd ,"devmem 0x%08X %d 0x%08X ",addr,len,val);
	printf("%s\n",cmd);
	system(cmd);
	status = system(cmd);
	if(-1 == status)
	{
		printf("system error\n");
	}
	usleep(1000);
}
static uint32_t memory_get(uint32_t addr )
{
	pid_t status;
	uint32_t val;
	uint8_t cmd[256];
	uint8_t result[1024];
	PRINT_FUNC_NAME;
	sprintf(cmd,"devmem 0x%08X ",addr);
	printf("%s\n",cmd);
    #if 1
	execute_cmd( cmd, result);
	val = strtoul(result,NULL,0);
    printf("read val:0x%08X\n", val );
    #else
    status = system(cmd);
    if(-1 == status)
    {
        printf("system error\n");
    }
#endif
	usleep(1000);
}
void Xil_Out32(uint32_t phyaddr, uint32_t val)
{
    memory_set(phyaddr,32,val);

}
uint32_t Xil_In32(uint32_t phyaddr)
{
    return memory_get(phyaddr);
}

#endif


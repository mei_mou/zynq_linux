#include<unistd.h>
#include<assert.h>
#include<string.h>
#include<sys/time.h>
#include<sys/types.h>
#include<errno.h>

#include "my_debug.h"

#include "board.h"
#include "usr_common.h"


void  
printbuf(void *buf,u32 len)
{
    u32 idx;
    PRINT_GREEN("buf:");
    for(idx=0 ; idx < len; idx++)
    {
        if(idx%16==0)
        {
            PRINT_GREEN("\n");
        }
        PRINT_GREEN(" %02X",((u8 *)buf)[idx]);
        
    }
    PRINT_GREEN("\n");
}


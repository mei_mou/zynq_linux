#ifndef __USR_COMMON_H__
#define __USR_COMMON_H__

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include<assert.h>

#include "my_typedef.h"
#include "adi_types.h"
#include "my_debug.h"
#include "board.h"



void  
printbuf(void *buf,u32 len);

#endif

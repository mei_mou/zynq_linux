
#ifndef __BOARD_H__
#define __BOARD_H__

#include "my_typedef.h"


#define SOFT_VER  "1.0.1"


#define LOW_LO              //TODO
#define RX_HIGH_IF          //TODO


/*dev path define */
#define DEVPATH_RCV_CMD  "/dev/ttyPS1"
//#define DEVPATH_RCV_CMD  "/dev/ttySGK1"


#define I2CDEV0 "/dev/i2c-0"
#define I2CDEV1 "/dev/i2c-1"

#define NO_PWR_HARDWARE (0)

#ifndef SHELL_CMD_EN
#define SHELL_CMD_EN (0)
#endif

#endif /*__BOARD_H__*/


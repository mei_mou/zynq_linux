#ifndef __XIL_IO__
#define __XIL_IO__



struct xil_mem_s{
    int fd;
    volatile uint8_t * map_base7c;
    volatile uint8_t * map_base50;
    volatile uint8_t * map_base51;
};
extern struct  xil_mem_s g_xil_mem;


extern void Xil_Out32(uint32_t phyaddr, uint32_t val); 
extern uint32_t Xil_In32(uint32_t phyaddr);
extern int xil_dev_mem_init();
extern int xil_dev_mem_deinit(void);    
#endif /*__XIL_IO__*/
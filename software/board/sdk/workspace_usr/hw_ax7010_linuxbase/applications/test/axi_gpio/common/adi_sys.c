/*!
*****************************************************************************
** \file        adi/src/adi_sys.c
**
** \brief       ADI system module function
**
** \attention   THIS SAMPLE CODE IS PROVIDED AS IS. xxx MICROELECTRONICS
**              ACCEPTS NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR
**              OMMISSIONS
**
** (C) Copyright 2013-2014 by xxx MICROELECTRONICS CO.,LTD
**
*****************************************************************************
*/

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <sys/mman.h>
#include <sys/time.h>
#include <sys/queue.h>
#include <sys/prctl.h>
#include <sys/syscall.h>
#include <linux/sched.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <time.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>
#include <mqueue.h>
#include <time.h>
#include <limits.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <sched.h>

#include <semaphore.h>
#include <basetypes.h>

#include "adi_types.h"
#include "adi_sys.h"

//*****************************************************************************
//*****************************************************************************
//** Local Defines
//*****************************************************************************
//*****************************************************************************

#define ADI_SYS_THREAD_NAME_SIZE 128
#define ADI_SYS_KERNEL_HZ      100

//*****************************************************************************
//*****************************************************************************
//** Local structures
//*****************************************************************************
//*****************************************************************************
/*
*******************************************************************************
** \brief thread with additional data field for this OS.
*******************************************************************************
*/
typedef struct {
    ADI_U32 pid;
    ADI_CHAR comm[32];
    ADI_CHAR task_state;
    ADI_U32 ppid;
    ADI_U64 utime;
    ADI_U64 stime;
    ADI_U64 cutime;
    ADI_U64 cstime;
    ADI_U64 start_time;
    ADI_U64 vsize;
    ADI_U64 rss;
    ADI_U32 priority;
    ADI_U32 policy;
    ADI_U32 avg_load;
    struct timeval tv;
} ADI_SYS_ThreadStatT;

typedef struct thread_node_t {
    ADI_U32 stackSize;
    ADI_U32 priority;
    ADI_VOID *function;
    ADI_VOID *optArg;
    ADI_CHAR optName[ADI_SYS_THREAD_NAME_SIZE];

    ADI_U32 thread_tid;
    ADI_SYS_ThreadStatT thread_stat;
    pthread_t thread_obj;
    pthread_attr_t thread_attr;
    pthread_mutex_t thread_mutex;
    pthread_cond_t thread_cond;
    pthread_mutex_t suspend_mutex;

     SLIST_ENTRY(thread_node_t) entries;
} adi_sys_linux_thread_t;

typedef struct {
    ADI_U32 inited;
    pthread_mutex_t mutexT;
} adi_sys_thread_controlT;

SLIST_HEAD(thread_head_t, thread_node_t) threadHead =
SLIST_HEAD_INITIALIZER(thread_head_t);

//*****************************************************************************
//*****************************************************************************
//** Global Data
//*****************************************************************************
//*****************************************************************************
ADI_U32 adiLogLevel = ADI_SYS_LOG_LEVEL_INFO;


//*****************************************************************************
//*****************************************************************************
//** Local Data
//*****************************************************************************
//*****************************************************************************


//*****************************************************************************
//*****************************************************************************
//** Local Functions Declaration
//*****************************************************************************
//*****************************************************************************


static ADI_VOID *sys_thread_task(ADI_VOID * thread);
static ADI_ERR obtain_thread_status(ADI_S32 pid,
    adi_sys_linux_thread_t * thread, ADI_SYS_ThreadStatT * thstat);

//*****************************************************************************
//*****************************************************************************
//** API Functions
//*****************************************************************************
//*****************************************************************************


ADI_ERR adi_sys_get_log_level(ADI_U32 * plogLevel)
{
    if (UNLIKELY(plogLevel == NULL)) {
        ADI_ERROR("[adi_sys_get_log_level] : NULL pointer");
        return ADI_SYS_ERR_BAD_PARAMETER;
    }
    *plogLevel = adiLogLevel;

    return ADI_OK;
}

ADI_ERR adi_sys_set_log_level(ADI_SYS_LogLevelEnumT logLevel)
{
    if (UNLIKELY((logLevel < ADI_SYS_LOG_LEVEL_ERROR) ||
            (logLevel > (ADI_SYS_LOG_LEVEL_NUM - 1)))) {
        ADI_ERROR("Invalid log level, please set it in the range of (0~%d).\n",
            (ADI_SYS_LOG_LEVEL_NUM - 1));
        return ADI_SYS_ERR_BAD_PARAMETER;
    }
    adiLogLevel = logLevel;

    return ADI_OK;
}

ADI_VOID *adi_sys_malloc(ADI_U32 size)
{
    ADI_VOID *mem = NULL;

    mem = malloc(size);

    return mem;

}

ADI_VOID adi_sys_free(ADI_VOID * ptr)
{
    free(ptr);
}

ADI_VOID *adi_sys_memcpy(ADI_VOID * dest, ADI_VOID * src, ADI_U32 size)
{
    return memcpy(dest, src, size);
}

ADI_VOID *adi_sys_memset(ADI_VOID * ptr, ADI_U8 c, ADI_U32 n)
{
    return memset(ptr, c, n);
}

ADI_SYS_SemHandleT adi_sys_sem_create(ADI_U32 initValue)
{
    sem_t *sem = NULL;

    sem = (sem_t *) adi_sys_malloc(sizeof(sem_t));
    if (UNLIKELY(sem == NULL)) {
        ADI_ERROR("adi_sys_sem_create: out of memory\n");
        return 0;
    }
    sem_init(sem, 0, initValue);

    return (ADI_SYS_SemHandleT) sem;
}

ADI_ERR adi_sys_sem_post(ADI_SYS_SemHandleT sem)
{
    ADI_ERR ret = ADI_OK;

    ret = sem_post((sem_t *) sem);
    if (UNLIKELY(ret == -1)) {
        ADI_ERROR("sem_post\n");
        return ADI_SYS_ERR_INVALID_SEMAPHORE_HANDLE;
    }
    return ADI_OK;
}

ADI_ERR adi_sys_sem_wait(ADI_SYS_SemHandleT sem)
{
    ADI_ERR ret;

    ret = sem_wait((sem_t *) sem);
    if (UNLIKELY(ret < 0)) {
        ADI_ERROR("sem_post\n");
        return ADI_SYS_ERR_INVALID_SEMAPHORE_HANDLE;
    }

    return ADI_OK;
}

ADI_ERR adi_sys_sem_wait_timeout(ADI_SYS_SemHandleT sem, ADI_U32 msecs)
{
    ADI_ERR ret;

    struct timespec tout;
    struct timeval ttval;

    gettimeofday(&ttval, 0);

    tout.tv_nsec = ttval.tv_usec * 1000 + (msecs % 1000) * 1000000;
    tout.tv_sec = ttval.tv_sec + msecs / 1000;
    ret = sem_timedwait((sem_t *) sem, &tout);

    if (UNLIKELY(ret < 0)) {
        return ADI_SYS_ERR_TIMEOUT;
    }
    return ADI_OK;
}

ADI_ERR adi_sys_sem_destroy(ADI_SYS_SemHandleT sem)
{
    ADI_ERR ret;

    ret = sem_destroy((sem_t *) sem);
    if (UNLIKELY(ret == -1)) {
        ADI_ERROR("sem_destroy\n");
        return ADI_SYS_ERR_INVALID_SEMAPHORE_HANDLE;
    }

    adi_sys_free((void *) sem);

    return ADI_OK;
}

ADI_VOID adi_sys_multi_task_start(ADI_VOID)
{
    while (LIKELY(1)) {
        sleep(1000);
    }
}

ADI_ERR adi_sys_thread_create(ADI_SYS_ThreadFunctionT func, ADI_VOID * arg,
    ADI_S32 priority, ADI_S32 ssize, const ADI_CHAR * name,
    ADI_SYS_ThreadHandleT * thread)
{
    ADI_U32 ret;
    adi_sys_linux_thread_t *linux_thread;

#if defined(_POSIX_THREAD_PRIORITY_SCHEDULING)
    struct sched_param thread_param;
#endif
    linux_thread = adi_sys_malloc(sizeof(adi_sys_linux_thread_t));
    if (UNLIKELY(linux_thread == NULL)) {
        return ADI_SYS_ERR_OUT_OF_MEMORY;
    }
    adi_sys_memset(linux_thread, 0, sizeof(adi_sys_linux_thread_t));

    ssize *= 1024;

    linux_thread->stackSize =
        (ssize > PTHREAD_STACK_MIN) ? ssize : PTHREAD_STACK_MIN;
    linux_thread->priority = priority;
    linux_thread->function = func;
    linux_thread->optArg = arg;
    if (LIKELY(name != NULL)) {
        strncpy(linux_thread->optName, name, sizeof(linux_thread->optName) - 2);
    }

    pthread_mutex_init(&linux_thread->suspend_mutex, NULL);
    pthread_mutex_init(&linux_thread->thread_mutex, NULL);
    pthread_cond_init(&linux_thread->thread_cond, NULL);

    ret = pthread_attr_init(&linux_thread->thread_attr);
    if (UNLIKELY(ret != 0)) {
        ADI_ERROR("ERROR: pthread_attr_init");
        goto ADI_SYS_CreateThread_error;
    }
#if defined(_POSIX_THREAD_ATTR_STACKSIZE)       /* need include unistd.h */
    if (LIKELY(ssize != 0)) {
        /*set stack size */
        if (UNLIKELY(pthread_attr_setstacksize(&linux_thread->thread_attr,
                    linux_thread->stackSize))) {
            pthread_attr_destroy(&linux_thread->thread_attr);
            ADI_ERROR("pthread_attr_setstacksize");
            goto ADI_SYS_CreateThread_error;
        }
    }
#endif

#if defined(_POSIX_THREAD_PRIORITY_SCHEDULING)
    pthread_attr_getschedparam(&linux_thread->thread_attr, &thread_param);
    switch (linux_thread->priority) {
        case (ADI_SYS_THREAD_PRIO_MIN + 1) ... ADI_SYS_THREAD_PRIO_MAX:
            if (LIKELY(!pthread_attr_setschedpolicy(&linux_thread->thread_attr,
                        SCHED_RR))) {
                thread_param.sched_priority = (int) (linux_thread->priority);
                pthread_attr_setschedparam(&linux_thread->thread_attr,
                    &thread_param);
#if defined(_POSIX_THREAD_PRIO_INHERIT)
                pthread_attr_setinheritsched(&linux_thread->thread_attr,
                    PTHREAD_EXPLICIT_SCHED);
#endif
            }
            break;
        case ADI_SYS_THREAD_PRIO_MIN:
        default:
            //pthread_attr_setschedpolicy(&linux_thread->thread_attr, SCHED_OTHER);
            break;
    }
#endif

    SLIST_INSERT_HEAD(&threadHead, linux_thread, entries);
    ret = pthread_create(&linux_thread->thread_obj, &linux_thread->thread_attr,
        sys_thread_task, linux_thread);
    if (UNLIKELY(ret != 0)) {
        pthread_attr_destroy(&linux_thread->thread_attr);
        SLIST_REMOVE(&threadHead, linux_thread, thread_node_t, entries);
        goto ADI_SYS_CreateThread_error;
    }

    gettimeofday(&linux_thread->thread_stat.tv, NULL);
    if (LIKELY(thread)) {
        *thread = (ADI_SYS_ThreadHandleT) linux_thread;
    }

    ADI_INFO("%s thread create OK\n", linux_thread->optName);


    return ADI_OK;

  ADI_SYS_CreateThread_error:
    pthread_mutex_destroy(&linux_thread->suspend_mutex);
    pthread_mutex_destroy(&linux_thread->thread_mutex);
    adi_sys_free(linux_thread);

    ADI_ERROR("adi_sys_thread_create() error");

    return ADI_SYS_ERR_THREAD_CREATE;
}

ADI_ERR adi_sys_thread_destroy(ADI_SYS_ThreadHandleT thread)
{
    adi_sys_linux_thread_t *linux_thread = (adi_sys_linux_thread_t *) thread;

    if (UNLIKELY(linux_thread == NULL)) {
        ADI_ERROR("ERROR: linux_thread=%x\n",
            (ADI_SYS_ThreadHandleT) linux_thread);
        return ADI_SYS_ERR_INVALID_HANDLE;
    }

    pthread_detach(linux_thread->thread_obj);
    if (UNLIKELY(pthread_cancel(linux_thread->thread_obj))) {
        ADI_ERROR("pthread_cancel");
        return ADI_SYS_ERR_BAD_PARAMETER;
    }

    pthread_attr_destroy(&linux_thread->thread_attr);
    pthread_mutex_destroy(&linux_thread->suspend_mutex);
    pthread_mutex_destroy(&linux_thread->thread_mutex);
    pthread_cond_destroy(&linux_thread->thread_cond);

    SLIST_REMOVE(&threadHead, linux_thread, thread_node_t, entries);

    adi_sys_free(linux_thread);

    return ADI_OK;
}

ADI_SYS_ThreadHandleT adi_sys_thread_self(ADI_VOID)
{
    adi_sys_linux_thread_t *handlePtr = NULL;
    pthread_t thread = pthread_self();

    SLIST_FOREACH(handlePtr, &threadHead, entries) {
        if (UNLIKELY(pthread_equal(handlePtr->thread_obj, thread)))
            break;
    }

    return (ADI_SYS_ThreadHandleT) handlePtr;
}

ADI_VOID adi_sys_thread_self_destroy(ADI_VOID)
{
    adi_sys_linux_thread_t *linux_thread = NULL;
    pthread_t thread_obj = pthread_self();

    pthread_detach(thread_obj);

    SLIST_FOREACH(linux_thread, &threadHead, entries) {
        if (UNLIKELY(pthread_equal(linux_thread->thread_obj, thread_obj)))
            break;
    }

    if (UNLIKELY(linux_thread == NULL)) {
        return;
    }

    pthread_attr_destroy(&linux_thread->thread_attr);
    pthread_mutex_destroy(&linux_thread->suspend_mutex);
    pthread_mutex_destroy(&linux_thread->thread_mutex);
    pthread_cond_destroy(&linux_thread->thread_cond);

    pthread_exit(NULL);
}

ADI_VOID adi_sys_thread_sleep(ADI_U32 msecs)
{
    usleep(1000 * msecs);
}

ADI_ERR adi_sys_thread_yield(ADI_VOID)
{
    return sched_yield();
}

ADI_VOID adi_sys_thread_suspend(ADI_SYS_ThreadHandleT thread)
{
    adi_sys_linux_thread_t *handlePtr = (adi_sys_linux_thread_t *) thread;

    if (UNLIKELY(handlePtr == NULL)) {
        ADI_ERROR("Params is NULL.");
        return;
    }
    /* Check processing signal SIGUSE2 by mutex lock. */
    if (LIKELY(!pthread_mutex_trylock(&handlePtr->suspend_mutex))) {
        if (UNLIKELY(pthread_kill(handlePtr->thread_obj, SIGUSR2))) {
            ADI_ERROR("System thread[0x%lx] suspend falid.",
                handlePtr->thread_obj);
        } else {
            pthread_mutex_unlock(&handlePtr->suspend_mutex);
        }
    }
}

ADI_VOID adi_sys_thread_self_suspend(ADI_VOID)
{
    adi_sys_linux_thread_t *handlePtr = NULL;
    pthread_t thread = pthread_self();

    SLIST_FOREACH(handlePtr, &threadHead, entries) {
        if (UNLIKELY(pthread_equal(handlePtr->thread_obj, thread)))
            break;
    }

    if (LIKELY((handlePtr != NULL) &&
            (pthread_mutex_trylock(&handlePtr->suspend_mutex) == 0))) {
        /* for pthread susupend , used SIGUSR2 */
        if (UNLIKELY(pthread_kill(handlePtr->thread_obj, SIGUSR2))) {
            ADI_ERROR("System thread[0x%lx] suspend falid.",
                handlePtr->thread_obj);
        } else {
            pthread_mutex_unlock(&handlePtr->suspend_mutex);
        }
    }
}

ADI_VOID adi_sys_thread_resume(ADI_SYS_ThreadHandleT thread)
{
    adi_sys_linux_thread_t *linux_thread = (adi_sys_linux_thread_t *) thread;

    if (UNLIKELY(linux_thread == NULL)) {
        ADI_ERROR("adi_sys_thread_resume() params error\n");
        return;
    }

    if (LIKELY(!pthread_mutex_trylock(&linux_thread->thread_mutex))) {
        if (UNLIKELY(pthread_cond_signal(&linux_thread->thread_cond))) {
            ADI_ERROR("System thread[0x%lx] suspend falid.",
                linux_thread->thread_obj);
        } else {
            pthread_mutex_unlock(&linux_thread->suspend_mutex);
        }
        pthread_mutex_unlock(&linux_thread->thread_mutex);
    } else {
        ADI_ERROR("System thread[0x%lx] suspend falid.",
            linux_thread->thread_obj);
    }
}

ADI_ERR adi_sys_wait_end_thread(ADI_SYS_ThreadHandleT thread)
{
    int ret;
    adi_sys_linux_thread_t *linux_thread = (adi_sys_linux_thread_t *) thread;

    if (UNLIKELY(linux_thread == NULL)) {
        ADI_ERROR("ERROR: linux_thread=%x\n",
            (ADI_SYS_ThreadHandleT) linux_thread);
        return ADI_ERR_INVALID_HANDLE;
    }

    ret = pthread_join(linux_thread->thread_obj, NULL);
    if (UNLIKELY(ret != 0)) {
        ADI_ERROR("adi_sys_wait_end_thread() error %d.", ret);
        return ADI_ERR_INVALID_HANDLE;
    }

    pthread_attr_destroy(&linux_thread->thread_attr);
    pthread_mutex_destroy(&linux_thread->thread_mutex);
    pthread_cond_destroy(&linux_thread->thread_cond);

    SLIST_REMOVE(&threadHead, linux_thread, thread_node_t, entries);

    adi_sys_free(linux_thread);

    return ADI_OK;
}

ADI_VOID adi_sys_thread_statistics(ADI_VOID)
{
    adi_sys_linux_thread_t *handlePtr = NULL;
    ADI_S32 pid = syscall(SYS_getpid);

    printf("\033[2J\033[0;0H");
    printf
        (" +-------+------+-------+---------+---------------+-------------------+\n");
    printf
        (" |  Tid  | Stat |  CPU  | Prioity |  Stack  Size  |    Thread Name    |\n");
    printf
        (" +-------+------+-------+---------+---------------+-------------------+\n");

    SLIST_FOREACH(handlePtr, &threadHead, entries) {
        obtain_thread_status(pid, handlePtr, &handlePtr->thread_stat);
        printf(" |\033[35m%6d\033[0m |"
            "   \033[36m%c\033[0m  |"
            "\033[33m%3d.%02d\033[0m%%|"
            "\033[32m%8d\033[0m |"
            " \033[33m%8d\033[0m KByte|"
            "\033[34m%18.18s\033[0m |\n",
            handlePtr->thread_tid,
            handlePtr->thread_stat.task_state,
            handlePtr->thread_stat.avg_load / 100,
            handlePtr->thread_stat.avg_load % 100,
            handlePtr->priority,
            handlePtr->stackSize >> 10, handlePtr->optName);
    }

    printf
        (" +-------+------+-------+---------+---------------+-------------------+\n");
}

ADI_VOID adi_sys_get_date(ADI_CHAR * str)
{
    time_t curr;
    struct tm *p;

    time(&curr);

    p = gmtime(&curr);
    sprintf(str, "%04d-%02d-%02d %02d:%02d:%02d",
        (1900 + p->tm_year), (1 + p->tm_mon), p->tm_mday,
        p->tm_hour, p->tm_min, p->tm_sec);
}


//*****************************************************************************
//*****************************************************************************
//** Local Functions
//*****************************************************************************
//*****************************************************************************

static ADI_ERR dump_by_file(const ADI_CHAR * file, ADI_CHAR buffer[],
    ADI_S32 bsize)
{
    ADI_S32 fd = -1;

    if (UNLIKELY(!file || !buffer)) {
        ADI_ERROR("Params error.");
        return -1;
    }

    if (UNLIKELY(access(file, R_OK | F_OK))) {
        ADI_ERROR("file(%s) is no exist or can not read.", file);
        return -2;
    }

    if (UNLIKELY((fd = open(file, O_RDONLY)) < 0)) {
        return -2;
    }

    read(fd, buffer, bsize);

    close(fd);
    return 0;
}

static ADI_ERR obtain_thread_status(ADI_S32 pid,
    adi_sys_linux_thread_t * thread, ADI_SYS_ThreadStatT * thstat)
{
    ADI_CHAR *ptxbuffer = NULL;
    ADI_CHAR pt_buffer[128];
    ADI_CHAR text_buffer[1024];
    ADI_U64 time_cnt_pre = 0, time_cnt_cur = 0;
    ADI_U64 time_use_pre = 0, time_use_cur = 0;

    time_use_pre = thstat->tv.tv_sec * ADI_SYS_KERNEL_HZ +
        (thstat->tv.tv_usec * ADI_SYS_KERNEL_HZ) / 1000000;
    time_cnt_pre = thstat->utime + thstat->stime +
        thstat->cutime + thstat->cstime;
    snprintf(pt_buffer, sizeof(pt_buffer), "/proc/%d/task/%d/stat",
        pid, thread->thread_tid);
    dump_by_file(pt_buffer, text_buffer, sizeof(text_buffer));
    gettimeofday(&thstat->tv, NULL);

    ptxbuffer = text_buffer;
    while (*ptxbuffer != '\0' && *ptxbuffer != '(')
        ptxbuffer++;
    while (*ptxbuffer != '\0' && *ptxbuffer != ')') {
        if (UNLIKELY(*ptxbuffer == ' '))
            *ptxbuffer = '_';
        ptxbuffer++;
    }

    sscanf(text_buffer, "%u %s %c %u %*u %*u %*u %*u "
        "%*u %*u %*u %*u %*u %Lu %Lu %Lu %Lu %*u %*u %*u %*u %Lu %Lu "
        "%Lu %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u %*u "
        "%*u %*u %u %u %*u %*u %*u %*u %*u %*u",
        &thstat->pid, thstat->comm, &thstat->task_state, &thstat->ppid,
        &thstat->utime, &thstat->stime, &thstat->cutime, &thstat->cstime,
        &thstat->start_time, &thstat->vsize, &thstat->rss, &thstat->priority,
        &thstat->policy);

    time_use_cur = thstat->tv.tv_sec * ADI_SYS_KERNEL_HZ +
        (thstat->tv.tv_usec * ADI_SYS_KERNEL_HZ) / 1000000;
    time_cnt_cur =
        thstat->utime + thstat->stime + thstat->cutime + thstat->cstime;
    thstat->avg_load =
        (time_cnt_cur - time_cnt_pre) * 10000 / ((time_use_cur -
            time_use_pre) ? : INT_MAX);
    return 0;
}

static ADI_VOID sys_thread_suspend(ADI_S32 sig)
{
    adi_sys_linux_thread_t *handlePtr = NULL;
    pthread_t thread = pthread_self();

    SLIST_FOREACH(handlePtr, &threadHead, entries) {
        if (UNLIKELY(pthread_equal(handlePtr->thread_obj, thread)))
            break;
    }

    if (UNLIKELY(handlePtr == NULL)) {
        ADI_ERROR("System thread[0x%lx] suspend error.", thread);
        return;
    }

    if (LIKELY(!pthread_mutex_trylock(&handlePtr->thread_mutex))) {
        if (UNLIKELY(pthread_cond_wait(&handlePtr->thread_cond,
                    &handlePtr->thread_mutex))) {
            ADI_ERROR("System thread[0x%lx] suspend falid.", thread);
        }
        pthread_mutex_unlock(&handlePtr->thread_mutex);
    }
}

static ADI_VOID *sys_thread_task(ADI_VOID * thread)
{
    adi_sys_linux_thread_t *handlePtr = (adi_sys_linux_thread_t *) thread;

    prctl(PR_SET_NAME, handlePtr->optName);
    handlePtr->thread_tid = syscall(SYS_gettid);
    signal(SIGUSR2, sys_thread_suspend);
    ((ADI_SYS_ThreadFunctionT) (handlePtr->function)) (handlePtr->optArg);
    pthread_exit(NULL);
}

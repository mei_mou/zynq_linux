/**************************************************************************
  * @brief        : 通过直接访问寄存器的方式测试axi_gpio
  * @author       : shiweisun@foxmail.com
  * @copyright    : NONE
  * @version      : 0.1
  * @note         :   
  * @history      : 
***************************************************************************/
#include "usr_common.h"
#include "xil_io.h"
/* Register addr_base Definitions */

#define XGPIO_DATA_addr_base	0x0 /* Data register */
#define XGPIO_TRI_addr_base	0x4 /* I/O direction register */
#define XGPIO_GIER_addr_base	0x11c /* Global Interrupt Enable */
#define XGPIO_GIER_IE		BIT(31)

#define XGPIO_IPISR_addr_base	0x120 /* IP Interrupt Status */
#define XGPIO_IPIER_addr_base	0x128 /* IP Interrupt Enable */

#define XGPIO_CHANNEL_addr_base	0x8

# define xgpio_readreg(addr)		Xil_In32(addr)
# define xgpio_writereg(addr, val)	Xil_Out32(addr,val) 

struct xgpio_instance {
	u32 gpio_state;
	u32 gpio_dir;
	u32 addr_base;
	int irq_base;
	u32 irq_enable;
	bool no_init;
};
struct xgpio_instance  g_axi_leds;


static int xgpio_dir_out( struct xgpio_instance *chip,unsigned int gpio, int val)
{
    PRINT_DEBUG("****%s \n",__func__);
	/* Write state of GPIO signal */
	if (val)
		chip->gpio_state |= BIT(gpio);
	else
		chip->gpio_state &= ~BIT(gpio);
	xgpio_writereg(chip->addr_base + XGPIO_DATA_addr_base,
		       chip->gpio_state);
	/* Clear the GPIO bit in shadow register and set direction as output */
	chip->gpio_dir &= ~BIT(gpio);
	xgpio_writereg(chip->addr_base + XGPIO_TRI_addr_base, chip->gpio_dir);


	return 0;
}
static int xgpio_dir_in(struct xgpio_instance *chip, unsigned int gpio)
{
	/* Set the GPIO bit in shadow register and set direction as input */
	chip->gpio_dir |= BIT(gpio);
	xgpio_writereg(chip->addr_base + XGPIO_TRI_addr_base, chip->gpio_dir);

	return 0;
}
static void xgpio_set(struct xgpio_instance *chip,unsigned int gpio, int val)
{
    PRINT_DEBUG("****%s \n",__func__);

	/* Write to GPIO signal and set its direction to output */
	if (val)
		chip->gpio_state |= BIT(gpio);
	else
		chip->gpio_state &= ~BIT(gpio);

	xgpio_writereg(chip->addr_base + XGPIO_DATA_addr_base,
							 chip->gpio_state);
	PRINT_DEBUG("Read addr:%08X val:%08X \n",
	chip->addr_base + XGPIO_DATA_addr_base,
	xgpio_readreg(chip->addr_base + XGPIO_DATA_addr_base));
}

static int xgpio_get(struct xgpio_instance *chip, unsigned int gpio)
{
    PRINT_DEBUG("****%s \n",__func__);

	return !!(xgpio_readreg(chip->addr_base + XGPIO_DATA_addr_base) & BIT(gpio));
}


void axi_leds_init(void)
{
	struct xgpio_instance *chip = &g_axi_leds;
	
	chip->addr_base = 0x41210000;
	chip->no_init = 1;
	if (chip->no_init) {
		chip->gpio_state = xgpio_readreg(chip->addr_base +
						 XGPIO_DATA_addr_base);
		chip->gpio_dir = xgpio_readreg(chip->addr_base + XGPIO_TRI_addr_base);
	} 
	
}

void gpio_test(void)
{
	struct xgpio_instance *chip = &g_axi_leds;
	u32 cnt =3;
	axi_leds_init();
	xgpio_dir_out( chip,0, 1);
	xgpio_dir_out( chip,1, 1);
	xgpio_dir_out( chip,2, 1);
	xgpio_dir_out( chip,3, 1);
	while(cnt--)
	{
		xgpio_set( chip,0, 1);
		xgpio_set( chip,1, 1);
		xgpio_set( chip,2, 1);
		xgpio_set( chip,3, 1);
		sleep(1);
		xgpio_set( chip,0, 0);
		xgpio_set( chip,1, 0);
		xgpio_set( chip,2, 0);
		xgpio_set( chip,3, 0);
		sleep(1);
	}
}
int main(void)
{
	u32 addr = 0x41210000;
	u32 val_wr,val_rd;
	gpio_test();
	while(1)
	{
		val_wr = 0x0f;
		Xil_Out32(addr,val_wr);
		val_rd = Xil_In32(addr);
		printf("addr:%08X,val_wr:%08X,val_rd:%08X\n",addr,val_wr,val_rd);
		val_rd = Xil_In32(addr+1);
		sleep(1);
		val_wr = 0;
		Xil_Out32(addr,val_wr);
		val_rd = Xil_In32(addr);
		printf("addr:%08X,val_wr:%08X,val_rd:%08X\n",addr,val_wr,val_rd);
		val_rd = Xil_In32(addr+1);
		sleep(1);
	}
}

/**************************************************************************
  * @brief        : linux  i2c 测试例程
  * @author       : shiweisun@foxmail.com
  * @copyright    : NONE
  * @version      : 0.1
  * @note         :   
  * @history      : 
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/epoll.h>

#include "adi_types.h"
//#include "adi_sys.h"
#include "adi_i2c.h"

int usr_i2c_init(ADI_SYS_HandleT *hd,ADI_CHAR *dev)
{
    ADI_ERR errorCode = ADI_OK; 
    ADI_I2C_OpenParam config;
    
    config.nodePath = dev; //device node
    config.slaveAddr = 0xA0;//slave addr
    
    *hd = adi_i2c_open(&errorCode, &config);
    if (errorCode != ADI_OK)
    {
        printf("adi_spi_open() failed.\n");
        return errorCode;
    }
    return 0;
}

int 
usr_i2c_exit(ADI_SYS_HandleT *hd)
{
    ADI_ERR errorCode = ADI_OK;
    if(*hd)
    {
        errorCode = adi_i2c_close(*hd);
        if (errorCode != ADI_OK)
        {
            printf("adi_i2c_close failed.\n");
            return errorCode;
        }
        else
        {
            printf("i2c closed.\n");
            *hd = NULL;
        }
    }
    return 0;
}


int 
usr_i2c_write(ADI_SYS_HandleT *hd,void * wr_buf, ADI_U32 wr_len)
{
    ADI_ERR ret = ADI_OK;
    printf("%s\n",__func__);
    ret = adi_i2c_write(*hd, wr_buf, wr_len);
    if(ADI_I2C_ERR_BAD_PARAMETER == ret)
    {
        printf("%s parame error.\n",__func__);
        return -1;
    }    
    else if (ret != ADI_OK)
    {
        printf("%s failed.\n",__func__);
        return ret;
    }
    return wr_len;
}
int 
usr_i2c_read(ADI_SYS_HandleT *hd,void * rd_buf, ADI_U32 rd_len)
{
    ADI_ERR ret = ADI_OK;
    printf("%s\n",__func__);
    ret = adi_i2c_read(*hd, rd_buf, rd_len);
    if(ADI_I2C_ERR_BAD_PARAMETER == ret)
    {
        printf("%s parame error.\n",__func__);
        return -1;
    }    
    else if (ret != ADI_OK)
    {
        printf("%s failed.\n",__func__);
        return ret;
    }
    return rd_len;
}


#define I2CDEV0 "/dev/i2c-0"
#define I2CDEV1 "/dev/i2c-1"
#define I2CDEV  I2CDEV1
int main(int argc, char * argv[])
{
	int i ;
	int cnt;
	char wr_buf[64]; 
	char rd_buf[64];

    
    static ADI_SYS_HandleT i2chd;
	printf("%s i2c test!,\n build time:%s-%s\n",__FILE__,__DATE__,__TIME__);
	
	if(0 != usr_i2c_init(&i2chd,I2CDEV))
	{
	    printf("device init error!\n");
	}

	/*init the buffer*/
	for(i=0;i<sizeof(wr_buf);i++)
	{
		wr_buf[i]=0x55+i;
	}
	memset(rd_buf,0,sizeof(rd_buf));
	
	cnt = 10;
	while(cnt--)
	{
		usr_i2c_write(&i2chd,(void * )&wr_buf,sizeof(wr_buf));
		sleep(1);
		usr_i2c_read(&i2chd,(void * )&rd_buf,sizeof(rd_buf));
		sleep(1);
	}
	
    usr_i2c_exit(&i2chd);
	return 0;
}

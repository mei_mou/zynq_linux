/*!
*****************************************************************************
** \file        adi/src/adi_i2c.c
**
** \brief       ADI i2c module function
**
** \attention   THIS SAMPLE CODE IS PROVIDED AS IS. ---- MICROELECTRONICS
**              ACCEPTS NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR
**              OMMISSIONS
**
** (C) Copyright 2013-2014 by ---- MICROELECTRONICS CO.,LTD
**
*****************************************************************************
*/

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include <linux/i2c-dev.h>
#include <linux/i2c.h>

//#include "adi_sys.h"
#include "adi_types.h"
#include "adi_i2c.h"

//*****************************************************************************
//*****************************************************************************
//** Local Defines
//*****************************************************************************
//*****************************************************************************
#define adi_sys_malloc malloc
#define adi_sys_memset memset
#define adi_sys_free free
#define ADI_ERROR printf
//*****************************************************************************
//*****************************************************************************
//** Local structures
//*****************************************************************************
//*****************************************************************************
typedef struct {
    /* File descriptor of device node. */
    ADI_U8 fdI2c;
    /*Address of slave. */
    ADI_U8 slaveAddr;
} ADI_I2C_HandleT;

//*****************************************************************************
//*****************************************************************************
//** Global Data
//*****************************************************************************
//*****************************************************************************

//*****************************************************************************
//*****************************************************************************
//** Local Data
//*****************************************************************************
//*****************************************************************************

#define I2C_M_TEN           0x0010      /* this is a ten bit chip address */
#define I2C_M_RD            0x0001      /* read data, from slave to master */
#define I2C_M_NOSTART       0x4000  /* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_REV_DIR_ADDR  0x2000  /* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_IGNORE_NAK    0x1000  /* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_NO_RD_ACK     0x0800  /* if I2C_FUNC_PROTOCOL_MANGLING */
#define I2C_M_RECV_LEN      0x0400  /* length will be first received byte */

//*****************************************************************************
//*****************************************************************************
//** Local Functions Declaration
//*****************************************************************************
//*****************************************************************************

ADI_ERR i2c_check_params(ADI_I2C_OpenParam * configPtr);
ADI_ERR i2c_set_config(ADI_I2C_HandleT * handlePtr,
    ADI_I2C_OpenParam * configPtr);

//*****************************************************************************
//*****************************************************************************
//** API Functions
//*****************************************************************************
//*****************************************************************************
ADI_SYS_HandleT adi_i2c_open(ADI_ERR * errorCodePtr,
    ADI_I2C_OpenParam * userConfig)
{

    ADI_I2C_HandleT *i2cHandle = NULL;

    /* get memory for i2cHandle */
    i2cHandle = adi_sys_malloc(sizeof(ADI_I2C_HandleT));
    if (i2cHandle == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        *errorCodePtr = ADI_I2C_ERR_OUT_OF_MEMORY;
        return NULL;
    }
    adi_sys_memset(i2cHandle, 0, sizeof(ADI_I2C_HandleT));

    /* paramter checkout */
    *errorCodePtr = i2c_check_params(userConfig);
    if (*errorCodePtr != ADI_OK) {
        ADI_ERROR("parameters error!\n");
        goto error_out;
    }
    /* get parameter for i2cHandle and set */
    *errorCodePtr = i2c_set_config(i2cHandle, userConfig);
    if (*errorCodePtr != ADI_OK) {
        ADI_ERROR("parameters error!\n");
        goto error_out;
    }

    return (ADI_SYS_HandleT) i2cHandle;

  error_out:
    adi_sys_free(i2cHandle);
    i2cHandle = NULL;
    return NULL;

}

ADI_ERR adi_i2c_close(ADI_SYS_HandleT handle)
{

    ADI_I2C_HandleT *handleOperate = (ADI_I2C_HandleT *) handle;

    if (handleOperate == NULL) {
        return ADI_I2C_ERR_BAD_PARAMETER;
    }
    close(handleOperate->fdI2c);
    adi_sys_free(handleOperate);
    handleOperate = NULL;

    return ADI_OK;

}

ADI_ERR adi_i2c_read(ADI_SYS_HandleT handle, void *data, ADI_U32 dataLen)
{

    ADI_S32 ret;
    void *rx = data;
    ADI_I2C_HandleT *handleOperate = (ADI_I2C_HandleT *) handle;

    if (handleOperate == NULL || rx == NULL || dataLen <= 0) {
        return ADI_I2C_ERR_BAD_PARAMETER;
    }

    ret = read(handleOperate->fdI2c, rx, dataLen);
    if (ret < 0 || ret != dataLen) {
        ADI_ERROR("I2C read error.\n");
        return ADI_I2C_ERR_READ_FAILED;
    }

    return ADI_OK;

}

ADI_ERR adi_i2c_write(ADI_SYS_HandleT handle, void *data, ADI_U32 dataLen)
{

    ADI_S32 ret;
    void *tx = data;
    ADI_I2C_HandleT *handleOperate = (ADI_I2C_HandleT *) handle;

    if (handleOperate == NULL || tx == NULL || dataLen <= 0) {
        return ADI_I2C_ERR_BAD_PARAMETER;
    }
    ret = write(handleOperate->fdI2c, tx, dataLen);
    if (ret < 0 || ret != dataLen) {
        ADI_ERROR("I2C write error.\n");
        return ADI_I2C_ERR_WRITE_FAILED;
    }

    return ADI_OK;

}

ADI_ERR adi_i2c_read_ioctl(ADI_SYS_HandleT handle, ADI_U8 addr,
    void *txData, ADI_U16 txDataLen, void *rxData, ADI_U16 rxDataLen)
{

    ADI_S32 ret;
    void *tx = txData;
    void *rx = rxData;
    ADI_I2C_HandleT *handleOperate = (ADI_I2C_HandleT *) handle;

    if (handleOperate == NULL || addr <= 0 || tx == NULL ||
        txDataLen <= 0 || rx == NULL || rxDataLen <= 0) {
        return ADI_I2C_ERR_BAD_PARAMETER;
    }
    struct i2c_rdwr_ioctl_data *ioctlData = NULL;

    ioctlData =
        (struct i2c_rdwr_ioctl_data *) adi_sys_malloc(sizeof(struct
            i2c_rdwr_ioctl_data));
    if (ioctlData == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        return ADI_I2C_ERR_OUT_OF_MEMORY;
    }
    ioctlData->nmsgs = 2;
    ioctlData->msgs =
        (struct i2c_msg *) adi_sys_malloc(ioctlData->nmsgs *
        sizeof(struct i2c_msg));
    if (ioctlData->msgs == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        free(ioctlData);
        return ADI_I2C_ERR_OUT_OF_MEMORY;
    }
    ioctlData->msgs[0].addr = addr;
    ioctlData->msgs[0].flags = 0;
    ioctlData->msgs[0].len = txDataLen;
    ioctlData->msgs[0].buf = tx;

    ioctlData->msgs[1].addr = addr;
    ioctlData->msgs[1].flags = I2C_M_RD;
    ioctlData->msgs[1].len = rxDataLen;
    ioctlData->msgs[1].buf = rx;

    ret = ioctl(handleOperate->fdI2c, I2C_RDWR, (unsigned long) ioctlData);
    if (ret <= 0) {
        ADI_ERROR("I2C error!\n");
        free(ioctlData->msgs);
        free(ioctlData);
        return ADI_I2C_ERR_FROM_DRIVER;
    }
    free(ioctlData->msgs);
    free(ioctlData);

    return ADI_OK;

}

ADI_ERR adi_i2c_write_ioctl(ADI_SYS_HandleT handle, ADI_U16 addr,
    void *data, ADI_U16 dataLen)
{

    ADI_S32 ret;
    void *tx = data;
    ADI_I2C_HandleT *handleOperate = (ADI_I2C_HandleT *) handle;

    if (handleOperate == NULL || addr <= 0 || tx == NULL || dataLen <= 0) {
        return ADI_I2C_ERR_BAD_PARAMETER;
    }
    struct i2c_rdwr_ioctl_data *ioctlData = NULL;

    ioctlData =
        (struct i2c_rdwr_ioctl_data *) adi_sys_malloc(sizeof(struct
            i2c_rdwr_ioctl_data));
    if (ioctlData == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        return ADI_I2C_ERR_OUT_OF_MEMORY;
    }
    ioctlData->nmsgs = 1;
    ioctlData->msgs =
        (struct i2c_msg *) adi_sys_malloc(ioctlData->nmsgs *
        sizeof(struct i2c_msg));
    if (ioctlData->msgs == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        free(ioctlData);
        return ADI_I2C_ERR_OUT_OF_MEMORY;
    }
    ioctlData->msgs->addr = addr;
    ioctlData->msgs->flags = 0;
    ioctlData->msgs->len = dataLen;
    ioctlData->msgs->buf = tx;

    ret = ioctl(handleOperate->fdI2c, I2C_RDWR, (unsigned long) ioctlData);
    if (ret <= 0) {
        ADI_ERROR("I2C error!\n");
        free(ioctlData->msgs);
        free(ioctlData);
        return ADI_I2C_ERR_FROM_DRIVER;
    }
    free(ioctlData->msgs);
    free(ioctlData);

    return ADI_OK;

}

ADI_ERR adi_i2c_general_ioctl(ADI_SYS_HandleT handle,
    ADI_I2C_OpenParamT * p)
{

    ADI_S16 i;
    ADI_S32 ret;
    ADI_I2C_OpenParamT *pData = p;
    ADI_I2C_HandleT *handleOperate = (ADI_I2C_HandleT *) handle;

    if (handleOperate == NULL || pData == NULL) {
        return ADI_I2C_ERR_BAD_PARAMETER;
    }
    struct i2c_rdwr_ioctl_data *ioctlData = NULL;

    ioctlData =
        (struct i2c_rdwr_ioctl_data *) adi_sys_malloc(sizeof(struct
            i2c_rdwr_ioctl_data));
    if (ioctlData == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        return ADI_I2C_ERR_OUT_OF_MEMORY;
    }
    ioctlData->nmsgs = pData->nMsgs;
    ioctlData->msgs =
        (struct i2c_msg *) adi_sys_malloc(ioctlData->nmsgs *
        sizeof(struct i2c_msg));
    if (ioctlData->msgs == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        free(ioctlData);
        return ADI_I2C_ERR_OUT_OF_MEMORY;
    }
    for (i = 0; i < ioctlData->nmsgs; i++) {
        ioctlData->msgs[i].addr = pData->pMsgs[i].addr;
        ioctlData->msgs[i].flags = pData->pMsgs[i].flags;
        ioctlData->msgs[i].len = pData->pMsgs[i].len;
        ioctlData->msgs[i].buf = pData->pMsgs[i].buf;
    }
    ret = ioctl(handleOperate->fdI2c, I2C_RDWR, (unsigned long) ioctlData);
    if (ret <= 0) {
        ADI_ERROR("I2C error!\n");
        free(ioctlData->msgs);
        free(ioctlData);
        return ADI_I2C_ERR_FROM_DRIVER;
    }
    free(ioctlData->msgs);
    free(ioctlData);

    return ADI_OK;

}

//*****************************************************************************
//*****************************************************************************
//** Local Functions
//*****************************************************************************
//*****************************************************************************

ADI_ERR i2c_check_params(ADI_I2C_OpenParam * configPtr)
{

    if (strcmp(configPtr->nodePath, "/dev/i2c-0") &&
        strcmp(configPtr->nodePath, "/dev/i2c-1") &&
        strcmp(configPtr->nodePath, "/dev/i2c-2")) {
        ADI_ERROR("nodePath error!\n");
        return ADI_I2C_ERR_BAD_PARAMETER;

    }
    if (configPtr->slaveAddr < 0) {
        ADI_ERROR("slaveAddr error!\n");
        return ADI_I2C_ERR_BAD_PARAMETER;

    }

    return ADI_OK;

}

ADI_ERR i2c_set_config(ADI_I2C_HandleT * handlePtr,
    ADI_I2C_OpenParam * configPtr)
{

    ADI_S32 ret;

    handlePtr->fdI2c = open(configPtr->nodePath, O_RDWR);
    if (handlePtr->fdI2c < 0) {
        ADI_ERROR("Open %s failed!\n", configPtr->nodePath);
        return ADI_I2C_ERR_FROM_DRIVER;

    }
    /* get parameter for spiHandle */
    //handlePtr->mode = configPtr->mode;
    handlePtr->slaveAddr = configPtr->slaveAddr;
#if 0
    /* set I2C mode */
    if (handlePtr->mode) {
        ret = ioctl(handlePtr->fdI2c, I2C_TENBIT, 1);
    } else {
        ret = ioctl(handlePtr->fdI2c, I2C_TENBIT, 0);
    }
    if (ret < 0) {
        ADI_ERROR("Set slaveAddr mode failed!\n");
        return ADI_I2C_ERR_FROM_DRIVER;

    }
#endif
    /*select slave by address of slave. */
    ret = ioctl(handlePtr->fdI2c, I2C_SLAVE, handlePtr->slaveAddr >> 1);
    if (ret < 0) {
        ADI_ERROR("Select slave failed!\n");
        return ADI_I2C_ERR_FROM_DRIVER;
    }

    return ADI_OK;

}

/*!
*****************************************************************************
** \file           ./adi/inc/adi_i2c.h
**
** \brief          adi i2c module porting.
**
** \attention   THIS SAMPLE CODE IS PROVIDED AS IS. ---- MICROELECTRONICS
**                  ACCEPTS NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR
**                  OMMISSIONS
**
** (C) Copyright 2013-2014 by ---- MICROELECTRONICS CO.,LTD
**
*****************************************************************************
*/

#ifndef _ADI_I2C_H_
#define _ADI_I2C_H_



//*****************************************************************************
//*****************************************************************************
//** Defines and Macros
//*****************************************************************************
//*****************************************************************************


/*
**************************************************************************
** Defines for general error codes of the module.
**************************************************************************
*/
/*! Bad parameter passed. */
#define ADI_I2C_ERR_BAD_PARAMETER                                          \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_BAD_PARAMETER)
/*! Memory allocation failed. */
#define ADI_I2C_ERR_OUT_OF_MEMORY                                          \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_OUT_OF_MEMORY)
/*! Device already initialised. */
#define ADI_I2C_ERR_ALREADY_INITIALIZED                                    \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_ALREADY_INITIALIZED)
/*! Device not initialised. */
#define ADI_I2C_ERR_NOT_INITIALIZED                                        \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_NOT_INITIALIZED)
/*! Feature or function is not available. */
#define ADI_I2C_ERR_FEATURE_NOT_SUPPORTED                                  \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_FEATURE_NOT_SUPPORTED)
/*! Timeout occured. */
#define ADI_I2C_ERR_TIMEOUT                                                \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_TIMEOUT)
/*! The device is busy, try again later. */
#define ADI_I2C_ERR_DEVICE_BUSY                                            \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_DEVICE_BUSY)
/*! Invalid handle was passed. */
#define ADI_I2C_ERR_INVALID_HANDLE                                         \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_INVALID_HANDLE)
/*! Semaphore could not be created. */
#define ADI_I2C_ERR_SEMAPHORE_CREATE                                       \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_SEMAPHORE_CREATE)
/*! The driver's used version is not supported. */
#define ADI_I2C_ERR_UNSUPPORTED_VERSION                                    \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_UNSUPPORTED_VERSION)
/*! The driver's used version is not supported. */
#define ADI_I2C_ERR_FROM_DRIVER                                            \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_FROM_DRIVER)
/*! The device/handle is not open.. */
#define ADI_I2C_ERR_NOT_OPEN                                               \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_NOT_OPEN)
/*! The file is written failed. */
#define ADI_I2C_ERR_WRITE_FAILED                                           \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_WRITE_FAILED)
/*! The file is read failed. */
#define ADI_I2C_ERR_READ_FAILED                                            \
                              (ADI_I2C_MODULE_BASE + ADI_ERR_READ_FAILED)


/*
*******************************************************************************
** \brief Configuration parameters for I2C.
*******************************************************************************
*/

typedef struct
{
    /* Device node number. */
    ADI_CHAR *nodePath;
    /*Address of slave.*/
    ADI_U8  slaveAddr;
}ADI_I2C_OpenParam;

/*
*******************************************************************************
** \brief Configuration parameters for messages.
*******************************************************************************
*/
typedef struct
{
    ADI_U8 addr;     /* slave address */
    ADI_U16 flags;    /* read/write flag */
    ADI_U16 len;      /* msg length */
    ADI_U8 *buf;      /* pointer to msg data */
}ADI_I2C_MsgT;

/*
*******************************************************************************
** \brief Configuration parameters for adi_i2c_general_ioctl();.
*******************************************************************************
*/
typedef struct
{
    ADI_I2C_MsgT *pMsgs;
    ADI_U8  nMsgs;  /* numbers of i2c_msg structure*/
}ADI_I2C_OpenParamT;

//*****************************************************************************
//*****************************************************************************
//** API Functions
//*****************************************************************************
//*****************************************************************************

#ifdef __cplusplus
extern "C" {
#endif


/*!
*******************************************************************************
** \brief Open one I2C ADI instance.
**
** \param[in] ErrorCode:  A Pointer to return the error code.
**
** \param[in] UserConfig: Contains I2C configuration parameters.
**
** \return
**         - #(ADI_SYS_HandleT)i2cHandle                  An valid handle of i2c ADI instance
**                                                                          when function calls success.
** \sa
**         - adi_i2c_close
**
*******************************************************************************
*/
ADI_SYS_HandleT adi_i2c_open(ADI_ERR *errorCodePtr, ADI_I2C_OpenParam *userConfig);

/*!
*******************************************************************************
** \brief Close I2C ADI instance.
**
** \param[in] handle: Valid I2C ADI instance handle previously opened by #adi_i2c_open.
**
** \return
**         - #ADI_OK                                                 On success.
**         - #ADI_I2C_ERR_BAD_PARAMETER                Invalid parameter.
**
** \sa
**         - adi_i2c_open
**
*******************************************************************************
*/
ADI_ERR adi_i2c_close(ADI_SYS_HandleT handle);

/*!
*******************************************************************************

** \brief Send data by I2C.
**
** \parame[in] handle: Valid I2C ADI instance handle previously opened by #adi_i2c_open.
**
** \parame[in] data:  A pointer to buffer user want to send.
**
** \parame[in] dataLen:  Length of data.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_I2C_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_I2C_ERR_WRITE_FAILED                     Data written failed.
**
** \sa
**         - adi_i2c_read
**
*******************************************************************************
*/
ADI_ERR adi_i2c_write(ADI_SYS_HandleT handle, void *data, ADI_U32 dataLen);

/*!
*******************************************************************************

** \brief Receive data by I2C.
**
** \parame[in] handle: Valid I2C ADI instance handle previously opened by #adi_i2c_open.
**
** \parame[in] data:  A pointer to buffer user want to send.
**
** \parame[in] dataLen:  Length of buffer.

**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_I2C_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_I2C_ERR_READ_FAILED                       Data received failed.
**
** \sa
**         - adi_i2c_write
*******************************************************************************
*/
ADI_ERR adi_i2c_read(ADI_SYS_HandleT handle, void *data, ADI_U32 dataLen);

/*!
*******************************************************************************

** \brief Some devices need a repeated start, send data and then receive data.
**
** \parame[in] handle: Valid I2C ADI instance handle previously opened by #adi_i2c_open.
**
** \parame[in] addr:  Slave address.
**
** \parame[in] txData:  A pointer to data user want to send.
**
** \parame[in] txdataLen: Length of txdata.
**
** \parame[in] rxData:  A pointer to buffer user want to receive.
**
** \parame[in] rxdataLen: Length of buffer.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_I2C_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_I2C_ERR_OUT_MEMORY                      Get memory failed.
**         - #ADI_I2C_ERR_FROM_DRIVER                      Can't set by I2C driver.
**
** \sa
**         - adi_i2c_open
*******************************************************************************
*/
ADI_ERR adi_i2c_read_ioctl(ADI_SYS_HandleT handle, ADI_U8 addr,
    void *txData, ADI_U16 txDataLen, void *rxData, ADI_U16 rxDataLen);

/*!
*******************************************************************************
** \brief Send data by I2C.
**
** \parame[in] handle: Valid I2C ADI instance handle previously opened by #adi_i2c_open.
**
** \parame[in] addr:  Slave address.
**
** \parame[in] data:  A pointer to buffer user want to send.
**
** \parame[in] dataLen:  Length of buffer.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_I2C_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_I2C_ERR_OUT_MEMORY                      Get memory failed.
**         - #ADI_I2C_ERR_FROM_DRIVER                      Can't set by I2C driver.
**
** \sa
**         - adi_i2c_open
**
*******************************************************************************
*/
ADI_ERR adi_i2c_write_ioctl(ADI_SYS_HandleT handle, ADI_U16 addr,
                                                void *data, ADI_U16 dataLen);

/*!
*******************************************************************************
** \brief General I2C operation ADI instance,
**
** \parame[in] handle: Valid I2C ADI instance handle previously opened by #adi_i2c_open.
**
** \parame[in] p:  A pointer to data structures user want to set.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_I2C_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_I2C_ERR_OUT_MEMORY                      Get memory failed.
**         - #ADI_I2C_ERR_FROM_DRIVER                      Can't set by I2C driver.
**
** \sa
**         - adi_i2c_open
**
*******************************************************************************
*/
ADI_ERR adi_i2c_general_ioctl(ADI_SYS_HandleT handle, ADI_I2C_OpenParamT *p);


#ifdef __cplusplus
    }
#endif



#endif /* _ADI_I2C_H_ */


/*!
*****************************************************************************
** \file           ./adi/inc/adi_gpio.h
**
** \brief         adi gpio module porting.
**
** \attention   THIS SAMPLE CODE IS PROVIDED AS IS. ---- MICROELECTRONICS
**                 ACCEPTS NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR
**                 OMMISSIONS
**
** (C) Copyright 2013-2014 by ---- MICROELECTRONICS CO.,LTD
**
*****************************************************************************
*/

#ifndef _ADI_GPIO_H_
#define _ADI_GPIO_H_


//*****************************************************************************
//*****************************************************************************
//** Defines and Macros
//*****************************************************************************
//*****************************************************************************

/*
**************************************************************************
** Defines for general error codes of the module.
**************************************************************************
*/
/*! Bad parameter passed. */
#define ADI_GPIO_ERR_BAD_PARAMETER                                          \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_BAD_PARAMETER)
/*! Memory allocation failed. */
#define ADI_GPIO_ERR_OUT_OF_MEMORY                                          \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_OUT_OF_MEMORY)
/*! Device already initialised. */
#define ADI_GPIO_ERR_ALREADY_INITIALIZED                                    \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_ALREADY_INITIALIZED)
/*! Device not initialised. */
#define ADI_GPIO_ERR_NOT_INITIALIZED                                        \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_NOT_INITIALIZED)
/*! Feature or function is not available. */
#define ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED                                  \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_FEATURE_NOT_SUPPORTED)
/*! Timeout occured. */
#define ADI_GPIO_ERR_TIMEOUT                                                \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_TIMEOUT)
/*! The device is busy, try again later. */
#define ADI_GPIO_ERR_DEVICE_BUSY                                            \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_DEVICE_BUSY)
/*! Invalid handle was passed. */
#define ADI_GPIO_ERR_INVALID_HANDLE                                         \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_INVALID_HANDLE)
/*! Semaphore could not be created. */
#define ADI_GPIO_ERR_SEMAPHORE_CREATE                                       \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_SEMAPHORE_CREATE)
/*! The driver's used version is not supported. */
#define ADI_GPIO_ERR_UNSUPPORTED_VERSION                                    \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_UNSUPPORTED_VERSION)
/*! The driver's used version is not supported. */
#define ADI_GPIO_ERR_FROM_DRIVER                                            \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_FROM_DRIVER)
/*! The device/handle is not open.. */
#define ADI_GPIO_ERR_NOT_OPEN                                               \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_NOT_OPEN)
/*! The file is written failed. */
#define ADI_GPIO_ERR_WRITE_FAILED                                           \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_WRITE_FAILED)
/*! The file is read failed. */
#define ADI_GPIO_ERR_READ_FAILED                                            \
                              (ADI_GPIO_MODULE_BASE + ADI_ERR_READ_FAILED)




enum GPIO_DIRECATION{
    GPIO_INPUT=0,
    GPIO_OUTPUT=1,
};

enum GPIO_VALUE{
    GPIO_LOW = 0,
    GPIO_HIGH = 1,
};

/*
*******************************************************************************
** \brief ADC channel type.
*******************************************************************************
*/
typedef enum
{
    ADI_GPIO_ADC_CHANNEL_ONE,
    ADI_GPIO_ADC_CHANNEL_TWO,
}ADI_GPIO_ADC_ChannelT;

/*
*******************************************************************************
** \brief Configuration parameters for GPIO.
*******************************************************************************
*/

typedef struct
{
    /* channel number */
    ADI_GPIO_ADC_ChannelT channel;
    /* adc number */
    ADI_U32 value;

}ADI_GPIO_AdcValue;



/*
*******************************************************************************
** \brief adc parameter.
*******************************************************************************
*/

typedef struct
{

    ADI_U8 num_gpio;
    /* level mode, 1 for negative 0 for positive */
    ADI_U8 active_low;
    /* output/input, 1/0 */
    ADI_U8 direction;
    /* high/low level, 1/0 in positive mode, 0/1 in negative mode */
    ADI_U8 value;
}ADI_GPIO_OpenParam;

//*****************************************************************************
//*****************************************************************************
//** API Functions
//*****************************************************************************
//*****************************************************************************

#ifdef __cplusplus
extern "C" {
#endif


/*!
*******************************************************************************
** \brief Open one GPIO ADI instance.
**
** \param[in] ErrorCode: A pointer to return the error code.
**
** \param[in] UserConfig: Contains GPIO parameters.
**
** \return
**         - #(ADI_SYS_HandleT)gpioHandle                  An valid handle of gpio ADI instance
**                                                                            when function calls success.
** \sa
**         - adi_gpio_close
**
*******************************************************************************
*/
ADI_SYS_HandleT adi_gpio_open(ADI_ERR *errorCodePtr,
                                        ADI_GPIO_OpenParam *userConfig);




/*!
*******************************************************************************
** \brief Close GPIO ADI instance.
**
** \param[in] handle: Valid GPIO ADI instance handle previously opened by #adi_gpio_open.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #NULL                                                         Error occurred.
**         - #ADI_GPIO_ERR_BAD_PARAMETER                Invalid parameter.
**         - #ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED  File of "export" doesn't exist.
**         - #ADI_GPIO_ERR_OUT_OF_MEMORY               Get memory failed!
**         - #ADI_GPIO_ERR_FROM_DRIVER                    Can't get file descriptor of "unexport"
**                                                                              or file doesn`t exist!by #open.
**         - #ADI_GPIO_ERR_WRITE_FAILED                   File written failed.
**
** \sa
**         - adi_gpio_open
**
*******************************************************************************
*/
ADI_ERR adi_gpio_close(ADI_SYS_HandleT handle);



/*!
*******************************************************************************
** \brief set output value to "1".
**
** \param[in] handle: Valid GPIO ADI instance handle previously opened by #adi_gpio_open.
**
** return
**        - #ADI_OK                                                    On success.
**        - #ADI_GPIO_ERR_BAD_PARAMETER                 Invalid parameter.
**        - #ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED   Direction is output mode.
**        - #ADI_GPIO_ERR_FROM_DRIVER                     Can't get file descriptor of "value"
**                                                                              by #open.
**        - #ADI_GPIO_ERR_WRITE_FAILED                    File written failed.
**
** \sa
**        - adi_gpio_clear
**
*******************************************************************************
*/
ADI_ERR adi_gpio_set(ADI_SYS_HandleT handle);




/*!
*******************************************************************************
** \brief set output value to "0".
**
** \param[in] handle: Valid GPIO ADI instance handle previously opened by #adi_gpio_open.
**
** \return
**        - #ADI_OK                                                    On success.
**        - #ADI_GPIO_ERR_BAD_PARAMETER                 Invalid parameter.
**        - #ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED   Direction is output mode.
**        - #ADI_GPIO_ERR_FROM_DRIVER                     Can't get file descriptor of "value" by
**                                                                              #open.
**        - #ADI_GPIO_ERR_WRITE_FAILED                    File written failed.
**
** \sa
**        - adi_gpio_set
**
*******************************************************************************
*/
ADI_ERR adi_gpio_clear(ADI_SYS_HandleT handle);




/*!
*******************************************************************************

** \brief Get value of high-low level.
**
** \parame[in] handle: Valid GPIO ADI instance handle previously opened by #adi_gpio_open.
**
** \parame[in] value:  A pointer to return the 1/0 that represent high-low level.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_GPIO_ERR_BAD_PARAMETER                Invalid parameter.
**         - #ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED  Direction is input mode.
**         - #ADI_GPIO_ERR_FROM_DRIVER                    Can't get file descriptor of "value" by
**                                                                              #open.
**         - #ADI_GPIO_ERR_READ_FAILED                     File read failed.
**
** \sa
**         - adi_gpio_open
**
*******************************************************************************
*/
ADI_ERR adi_gpio_read_value(ADI_SYS_HandleT handle, ADI_S32 *value);


/*!
*******************************************************************************

** \brief Get value of adc channel.
**
** \parame[out] adcValue: adc channel and channel's value
**

**
** \return
**         - #ADI_OK                                     On success.
**         - #ADI_GPIO_ERR_BAD_PARAMETER                 Invalid parameter.
**         - #ADI_GPIO_ERR_FROM_DRIVER                   Read error
**
**
** \sa
**
*******************************************************************************
*/
ADI_ERR adi_gpio_read_adc(ADI_GPIO_AdcValue *adcValue);

#ifdef __cplusplus
    }
#endif



#endif /* _ADI_GPIO_H_ */

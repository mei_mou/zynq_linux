
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/epoll.h>

#include "adi_types.h"
//#include "adi_sys.h"
#include "adi_gpio.h"

/***********************local define*****************************************/
struct usr_gpio_info{
    ADI_SYS_HandleT hd_led0;
    ADI_SYS_HandleT hd_led1;
    ADI_SYS_HandleT hd_led2;
    ADI_SYS_HandleT hd_led3;
};


/***************************local function***********************************************/
static int 
usr_gpio_init(struct usr_gpio_info *host)
{
    ADI_ERR errorCode = ADI_OK;    
    ADI_GPIO_OpenParam config;
    
    printf("%s\n",__func__);
    /*init gpiox as output*/
    config.num_gpio = 28;
    config.direction = GPIO_OUTPUT;
    config.value = GPIO_LOW;
    config.active_low = 0;

    host->hd_led0 = adi_gpio_open(&errorCode, &config);
    if (errorCode != ADI_OK)
    {
        printf("gpio%d init failed.\n",config.num_gpio);
        goto error;
    }

    /*init gpiox as output*/
    config.num_gpio = 36;
    config.direction = GPIO_OUTPUT;
    config.value = GPIO_LOW;
    config.active_low = 0;

    host->hd_led1 = adi_gpio_open(&errorCode, &config);
    if (errorCode != ADI_OK)
    {
        printf("gpio%d init failed.\n",config.num_gpio);
        goto error;
    }
    
    /*init gpiox as output*/
    config.num_gpio = 37;
    config.direction = GPIO_OUTPUT;
    config.value = GPIO_LOW;
    config.active_low = 0;

    host->hd_led2 = adi_gpio_open(&errorCode, &config);
    if (errorCode != ADI_OK)
    {
        printf("gpio%d init failed.\n",config.num_gpio);
        goto error;
    }
    
    /*init gpiox as output*/
    config.num_gpio = 38;
    config.direction = GPIO_OUTPUT;
    config.value = GPIO_LOW;
    config.active_low = 0;

    host->hd_led3 = adi_gpio_open(&errorCode, &config);
    if (errorCode != ADI_OK)
    {
        printf("gpio%d init failed.\n",config.num_gpio);
        goto error;
    }
    

    return 0;
    error:
    if(NULL != host->hd_led0)
    {
        adi_gpio_close(host->hd_led0);
    }
    if(NULL != host->hd_led1)
    {
        adi_gpio_close(host->hd_led1);
    }
    if(NULL != host->hd_led2)
    {
        adi_gpio_close(host->hd_led2);
    }
    if(NULL != host->hd_led3)
    {
        adi_gpio_close(host->hd_led3);
    }
    
    return -1;
}

static int 
usr_gpio_exit(struct usr_gpio_info *host)
{
    ADI_ERR errorCode = ADI_OK;
    printf("%s\n",__func__);
    if(NULL != host->hd_led0)
    {
        adi_gpio_close(host->hd_led0);
    }
    if(NULL != host->hd_led1)
    {
        adi_gpio_close(host->hd_led1);
    }
    if(NULL != host->hd_led2)
    {
        adi_gpio_close(host->hd_led2);
    }
    if(NULL != host->hd_led3)
    {
        adi_gpio_close(host->hd_led3);
    }
    return 0;
}

static int 
usr_gpio_test(struct usr_gpio_info *host)
{
    printf("%s\n",__func__);
    int cnt = 10000;
    while(cnt--)
    {
        usleep(1000);        
        adi_gpio_set(host->hd_led0);
        adi_gpio_set(host->hd_led1);
        adi_gpio_set(host->hd_led2);
        adi_gpio_set(host->hd_led3);\
        usleep(1000);
        adi_gpio_clear(host->hd_led0);
        adi_gpio_clear(host->hd_led1);
        adi_gpio_clear(host->hd_led2);
        adi_gpio_clear(host->hd_led3);
    }
}


int main(int argc, char * argv[])
{
	int err = 0;
	struct usr_gpio_info gpio_cfg;
    
	printf("%s, build time:%s-%s\n",__FILE__,__DATE__,__TIME__);
    memset(&gpio_cfg,0,sizeof(gpio_cfg));
    if(0 != usr_gpio_init(&gpio_cfg))
    {
        printf("gpio init failed!\n");
        err = -1;
        goto error;
    }
    usr_gpio_test(&gpio_cfg);

    error:
    usr_gpio_exit(&gpio_cfg);
	return err;
}

/*!
*****************************************************************************
** \file            adi/src/adi_gpio.c
**
** \brief          ADI gpio module function
**
** \attention   THIS SAMPLE CODE IS PROVIDED AS IS. ---- MICROELECTRONICS
**                 ACCEPTS NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR
**                 OMMISSIONS
**
** (C) Copyright 2013-2014 by ---- MICROELECTRONICS CO.,LTD
**
*****************************************************************************
*/

#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <errno.h>
#include <unistd.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

//#include"adi_sys.h"
#include"adi_types.h"
#include"adi_gpio.h"

//*****************************************************************************
//*****************************************************************************
//** Local Defines

//*****************************************************************************
//*****************************************************************************
#define adi_sys_malloc malloc
#define adi_sys_memset memset
#define adi_sys_free free
#define ADI_ERROR printf
#define ADI_INFO printf

/* /sys/class/gpio PATH */
#define GPIO_EXPORT_PATH            "/sys/class/gpio/export"
#define GPIO_ACTIVE_LOW_PATH        "/sys/class/gpio/gpio%d/active_low"
#define GPIO_DIRECTION_PATH         "/sys/class/gpio/gpio%d/direction"
#define GPIO_VALUE_PATH             "/sys/class/gpio/gpio%d/value"
#define GPIO_UNEXPORT_PATH          "/sys/class/gpio/unexport"

#define GPIO_OPERATION_PATH         "/sys/class/gpio/gpio%d"

#define GK_ADC_IOC_MAGIC             'A'
#define IOC_ADC_SET_CHANNEL         _IOW(GK_ADC_IOC_MAGIC, 0, unsigned int)
#define IOC_ADC_GET_CHANNEL         _IOR(GK_ADC_IOC_MAGIC, 1, unsigned int)
#define IOC_ADC_GET_DATA            _IOR(GK_ADC_IOC_MAGIC, 2, unsigned int)

/*---------------------------------------------------------------------------*/
/* types, enums and structures                                               */
/*---------------------------------------------------------------------------*/
/*!
*******************************************************************************
**
** \brief ADC status parameter
**
**
*/

typedef struct {
    ADI_GPIO_ADC_ChannelT channel;
    ADI_ULONG data;
} ADI_GPIO_ADC_Status;

//*****************************************************************************
//*****************************************************************************
//** Local structures
//*****************************************************************************
//*****************************************************************************

typedef struct {
    /* file descriptor of export */
    ADI_S32 fdExport;
    /* file descriptor of unexport */
    ADI_S32 fdUnexport;
    /* file descriptor of active_low */
    ADI_S32 fdActiveLow;
    /* file descriptor of direction */
    ADI_S32 fdDirection;
    /* file descriptor of value */
    ADI_S32 fdValue;

    /* GPIO number */
    ADI_U8 gpioNum;
    /* level mode, 1/0 */
    ADI_U8 active_low;
    /* output/input, 1/0 */
    ADI_U8 direction;
    /*high/low level, 1/0 */
    ADI_U8 value;
} ADI_GPIO_HandleT;

//*****************************************************************************
//*****************************************************************************
//** Global Data
//*****************************************************************************
//*****************************************************************************

//*****************************************************************************
//*****************************************************************************
//** Local Data
//*****************************************************************************
//*****************************************************************************

//*****************************************************************************
//*****************************************************************************
//** Local Functions Declaration
//*****************************************************************************
//*****************************************************************************
static ADI_ERR gpio_check_params(ADI_GPIO_OpenParam * configPtr);
static ADI_ERR gpio_set_config(ADI_GPIO_HandleT * handlePtr,
    ADI_GPIO_OpenParam * configPtr);

//*****************************************************************************
//*****************************************************************************
//** API Functions
//*****************************************************************************
//*****************************************************************************

ADI_SYS_HandleT adi_gpio_open(ADI_ERR * errorCodePtr,
    ADI_GPIO_OpenParam * userConfig)
{

    ADI_GPIO_HandleT *gpioHandle = NULL;

    /* get memory for gpioHandle */
    gpioHandle = adi_sys_malloc(sizeof(ADI_GPIO_HandleT));
    if (gpioHandle == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        *errorCodePtr = ADI_GPIO_ERR_OUT_OF_MEMORY;
        return NULL;
    }
    adi_sys_memset(gpioHandle, 0, sizeof(ADI_GPIO_HandleT));

    /* parameter checkout */
    *errorCodePtr = gpio_check_params(userConfig);
    if (*errorCodePtr != ADI_OK) {
        ADI_ERROR("parameters error!\n");
        goto error_out;
    }

    /* get parameter for gpioHandle and set */
    *errorCodePtr = gpio_set_config(gpioHandle, userConfig);
    if (*errorCodePtr != ADI_OK) {
        ADI_ERROR("set parameters faild!\n");
        goto error_out;
    }

    return (ADI_SYS_HandleT) gpioHandle;

  error_out:
    adi_sys_free(gpioHandle);
    gpioHandle = NULL;
    return NULL;

}

ADI_ERR adi_gpio_close(ADI_SYS_HandleT handle)
{

    ADI_GPIO_HandleT *handleOperate = (ADI_GPIO_HandleT *) handle;

    if (handleOperate == NULL) {
        return ADI_ERR_BAD_PARAMETER;
    }
    ADI_S32 ret = 0;
    ADI_CHAR str[3];
    ADI_CHAR buf[50];

    adi_sys_memset(buf, 0, sizeof(buf));
    adi_sys_memset(str, 0, sizeof(str));
    snprintf(str, sizeof(str), "%d", handleOperate->gpioNum);
    if (access(GPIO_UNEXPORT_PATH, F_OK) != 0) {
        ADI_ERROR("%s doesn`t exist!\n", GPIO_UNEXPORT_PATH);
        return ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED;
    }
    snprintf(buf, sizeof(buf), GPIO_OPERATION_PATH, handleOperate->gpioNum);
    if (access(buf, F_OK) == 0) {
        handleOperate->fdUnexport = open(GPIO_UNEXPORT_PATH, O_WRONLY);
        if (handleOperate->fdUnexport < 0) {
            ADI_ERROR("Open %s failed!\n", GPIO_UNEXPORT_PATH);
            return ADI_GPIO_ERR_FROM_DRIVER;
        }
        ret = write(handleOperate->fdUnexport, str, 2);
        if (ret < 0) {
            ADI_ERROR("Write unexport error!\n");
            close(handleOperate->fdUnexport);
            return ADI_GPIO_ERR_WRITE_FAILED;
        }
        close(handleOperate->fdUnexport);
    } else {
        ADI_INFO("%s doesn`t exist!.\n", buf);
        return ADI_GPIO_ERR_FROM_DRIVER;
    }
    adi_sys_free(handleOperate);
    handleOperate = NULL;
    return ADI_OK;

}

ADI_ERR adi_gpio_set(ADI_SYS_HandleT handle)
{

    ADI_S32 ret = 0;
    ADI_CHAR buf[50];

    adi_sys_memset(buf, 0, sizeof(buf));
    ADI_GPIO_HandleT *handleOperate = (ADI_GPIO_HandleT *) handle;

    if (handleOperate == NULL) {
        return ADI_ERR_BAD_PARAMETER;
    }
    /*if GPIO is in output mode, value can be set! */
    if (handleOperate->direction == 1) {
        snprintf(buf, sizeof(buf), GPIO_VALUE_PATH, handleOperate->gpioNum);
        handleOperate->fdValue = open(buf, O_RDWR);
        if (handleOperate->fdValue < 0) {
            ADI_ERROR("Open %s failed!\n", buf);
            return ADI_GPIO_ERR_FROM_DRIVER;
        }
        ret = write(handleOperate->fdValue, "1", 1);
        if (ret < 0) {
            ADI_ERROR("Write value error.\n");
            close(handleOperate->fdValue);
            return ADI_GPIO_ERR_WRITE_FAILED;
        }
        handleOperate->value = 1;
        close(handleOperate->fdValue);
    } else {
        ADI_INFO("value can not be written!\n");
        return ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED;
    }

    return ADI_OK;

}

ADI_ERR adi_gpio_clear(ADI_SYS_HandleT handle)
{

    ADI_S32 ret = 0;
    ADI_CHAR buf[50];

    adi_sys_memset(buf, 0, sizeof(buf));
    ADI_GPIO_HandleT *handleOperate = (ADI_GPIO_HandleT *) handle;

    if (handleOperate == NULL) {
        return ADI_ERR_BAD_PARAMETER;
    }
    /*if GPIO is in output mode, value can be set! */
    if (handleOperate->direction == 1) {
        snprintf(buf, sizeof(buf), GPIO_VALUE_PATH, handleOperate->gpioNum);
        handleOperate->fdValue = open(buf, O_RDWR);
        if (handleOperate->fdValue < 0) {
            ADI_ERROR("Open %s failed!\n", buf);
            return ADI_GPIO_ERR_FROM_DRIVER;
        }
        ret = write(handleOperate->fdValue, "0", 1);
        if (ret < 0) {
            ADI_ERROR("Write value error.\n");
            close(handleOperate->fdValue);
            return ADI_GPIO_ERR_WRITE_FAILED;
        }
        handleOperate->value = 1;
        close(handleOperate->fdValue);
    } else {
        ADI_INFO("value can not be written!\n");
        return ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED;
    }

    return ADI_OK;

}

/******************************* Get GPIO  State ********************************/
ADI_ERR adi_gpio_read_value(ADI_SYS_HandleT handle, ADI_S32 * value)
{

    ADI_S32 ret = 0;
    ADI_CHAR buf[50];
    ADI_CHAR valueStr[2];

    adi_sys_memset(valueStr, 0, sizeof(valueStr));
    adi_sys_memset(buf, 0, sizeof(buf));
    ADI_GPIO_HandleT *handleOperate = (ADI_GPIO_HandleT *) handle;

    if (handleOperate == NULL) {
        return ADI_ERR_BAD_PARAMETER;
    }
    /*if GPIO is in input mode, value can be read! */
    if (handleOperate->direction == 0) {

        snprintf(buf, sizeof(buf), GPIO_VALUE_PATH, handleOperate->gpioNum);
        handleOperate->fdValue = open(buf, O_RDWR);
        if (handleOperate->fdValue < 0) {
            ADI_ERROR("Open %s failed!\n", buf);
            return ADI_GPIO_ERR_FROM_DRIVER;
        }
        ret = read(handleOperate->fdValue, valueStr, 1);
        if (ret < 0) {
            ADI_ERROR("Read value error.\n");
            close(handleOperate->fdValue);
            return ADI_ERR_READ_FAILED;
        }
        *value = atoi(valueStr);
        close(handleOperate->fdValue);
    } else {
        ADI_INFO("value can not be read!\n");
        return ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED;
    }

    return ADI_OK;
}

ADI_ERR adi_gpio_read_adc(ADI_GPIO_AdcValue * adcValue)
{
    static int adcHandle = 0;

    if (adcHandle == 0) {
        adcHandle = open("/dev/adc", O_RDWR);
        if (adcHandle == 0) {
            return ADI_ERR_FROM_DRIVER;
        }
    }
    if (adcValue == NULL) {
        return ADI_ERR_BAD_PARAMETER;
    }
    if (adcValue->channel != ADI_GPIO_ADC_CHANNEL_ONE &&
        adcValue->channel != ADI_GPIO_ADC_CHANNEL_TWO) {
        ADI_ERROR("ADC channel error.\n");
        return ADI_ERR_BAD_PARAMETER;
    }
    ioctl(adcHandle, IOC_ADC_GET_DATA, adcValue);

    return ADI_OK;
}

//*****************************************************************************
//*****************************************************************************
//** Local Functions
//*****************************************************************************
//*****************************************************************************

ADI_ERR gpio_check_params(ADI_GPIO_OpenParam * configPtr)
{

    if ((configPtr->num_gpio < 0) || (configPtr->num_gpio > 95)) {
        ADI_ERROR("num_gpio error.\n");
        goto error_out;
    }
    if ((configPtr->active_low != 1) && (configPtr->active_low != 0)) {
        ADI_ERROR("active_low error.\n");
        goto error_out;
    }
    if ((configPtr->direction != 1) && (configPtr->direction != 0)) {
        ADI_ERROR("direction error.\n");
        goto error_out;
    }
    if ((configPtr->value != 1) && (configPtr->value != 0)) {
        ADI_ERROR("value error.\n");
        goto error_out;
    }

    return ADI_OK;

  error_out:
    return ADI_GPIO_ERR_BAD_PARAMETER;

}

ADI_ERR gpio_set_config(ADI_GPIO_HandleT * handlePtr,
    ADI_GPIO_OpenParam * configPtr)
{

    ADI_S32 ret;

    ADI_CHAR str[3];
    ADI_CHAR buf[50];

    adi_sys_memset(str, 0, sizeof(str));
    adi_sys_memset(buf, 0, sizeof(buf));

    handlePtr->gpioNum = configPtr->num_gpio;
    handlePtr->active_low = configPtr->active_low;
    handlePtr->direction = configPtr->direction;
    handlePtr->value = configPtr->value;

    /*get file descriptor of GPIO and set */
    snprintf(str, sizeof(str), "%d", handlePtr->gpioNum);
    if (access(GPIO_EXPORT_PATH, F_OK) != 0) {
        ADI_ERROR("%s doesn`t exist!\n", GPIO_EXPORT_PATH);
        return ADI_GPIO_ERR_FEATURE_NOT_SUPPORTED;
    }

    snprintf(buf, sizeof(buf), GPIO_OPERATION_PATH, handlePtr->gpioNum);
    if (access(buf, F_OK) != 0) {
        handlePtr->fdExport = open(GPIO_EXPORT_PATH, O_WRONLY);
        if (handlePtr->fdExport < 0) {
            ADI_ERROR("Open %s failed!.\n", GPIO_EXPORT_PATH);
            return ADI_GPIO_ERR_FROM_DRIVER;
        }
        ret = write(handlePtr->fdExport, str, 2);
        if (ret < 0) {
            ADI_ERROR("Write %s error!\n", GPIO_EXPORT_PATH);
            close(handlePtr->fdExport);
            return ADI_GPIO_ERR_WRITE_FAILED;
        }
        close(handlePtr->fdExport);
    } else {
        ADI_INFO("%s exist!\n", buf);
    }
    adi_sys_memset(str, 0, sizeof(str));
    adi_sys_memset(buf, 0, sizeof(buf));

    /*get file descriptor of active_low and configure */
    snprintf(buf, sizeof(buf), GPIO_ACTIVE_LOW_PATH, handlePtr->gpioNum);
    handlePtr->fdActiveLow = open(buf, O_RDWR);
    if (handlePtr->fdActiveLow < 0) {
        ADI_ERROR("Open %s failed!\n", buf);
        return ADI_GPIO_ERR_FROM_DRIVER;
    }
    if (handlePtr->active_low == 1)
        ret = write(handlePtr->fdActiveLow, "1", 1);
    else
        ret = write(handlePtr->fdActiveLow, "0", 1);
    if (ret < 0) {
        ADI_ERROR("Write %s error.\n", buf);
        close(handlePtr->fdActiveLow);
        return ADI_GPIO_ERR_WRITE_FAILED;
    }
    close(handlePtr->fdActiveLow);
    adi_sys_memset(buf, 0, sizeof(buf));

    /*get file descriptor of direction and configure */
    snprintf(buf, sizeof(buf), GPIO_DIRECTION_PATH, handlePtr->gpioNum);
    handlePtr->fdDirection = open(buf, O_RDWR);
    if (handlePtr->fdDirection < 0) {
        ADI_ERROR("Open file %s failed!\n", buf);
        return ADI_GPIO_ERR_FROM_DRIVER;
    }
    if (handlePtr->direction == 1)
        if (handlePtr->value == 1)
            ret = write(handlePtr->fdDirection, "high", 4);
        else
            ret = write(handlePtr->fdDirection, "low", 3);
    else {
        if (handlePtr->value == 1)
            ret = write(handlePtr->fdDirection, "in1", 3);
        else
            ret = write(handlePtr->fdDirection, "in0", 3);
    }
    if (ret < 0) {
        ADI_ERROR("Write %s error.\n", buf);
        close(handlePtr->fdDirection);
        return ADI_GPIO_ERR_WRITE_FAILED;
    }
    close(handlePtr->fdDirection);
    adi_sys_memset(buf, 0, sizeof(buf));

    /*get file descriptor of value and configure */
    if (handlePtr->direction == 1) {
        snprintf(buf, sizeof(buf), GPIO_VALUE_PATH, handlePtr->gpioNum);
        handlePtr->fdValue = open(buf, O_RDWR);
        if (handlePtr->fdValue < 0) {
            ADI_ERROR("Open file %s failed!\n", buf);
            return ADI_GPIO_ERR_FROM_DRIVER;
        }
        if (handlePtr->value == 1)
            ret = write(handlePtr->fdValue, "1", 1);
        else
            ret = write(handlePtr->fdValue, "0", 1);
        if (ret < 0) {
            ADI_ERROR("Write %s error.\n", buf);
            close(handlePtr->fdValue);
            return ADI_GPIO_ERR_WRITE_FAILED;
        }
        close(handlePtr->fdValue);
        adi_sys_memset(buf, 0, sizeof(buf));
    }

    return ADI_OK;

}

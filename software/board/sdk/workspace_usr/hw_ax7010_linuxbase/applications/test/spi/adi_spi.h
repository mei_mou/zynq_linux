/*!
*****************************************************************************
** \file           ./adi/inc/adi_spi.h
**
** \brief          adi spi module porting.
**
** \attention   THIS SAMPLE CODE IS PROVIDED AS IS. ---- MICROELECTRONICS
**                  ACCEPTS NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR
**                  OMMISSIONS
**
** (C) Copyright 2013-2014 by ---- MICROELECTRONICS CO.,LTD
**
*****************************************************************************
*/

#ifndef _ADI_SPI_H_
#define _ADI_SPI_H_



//*****************************************************************************
//*****************************************************************************
//** Defines and Macros
//*****************************************************************************
//*****************************************************************************

/*
**************************************************************************
** Defines for general error codes of the module.
**************************************************************************
*/
/*! Bad parameter passed. */
#define ADI_SPI_ERR_BAD_PARAMETER                                          \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_BAD_PARAMETER)
/*! Memory allocation failed. */
#define ADI_SPI_ERR_OUT_OF_MEMORY                                          \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_OUT_OF_MEMORY)
/*! Device already initialised. */
#define ADI_SPI_ERR_ALREADY_INITIALIZED                                    \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_ALREADY_INITIALIZED)
/*! Device not initialised. */
#define ADI_SPI_ERR_NOT_INITIALIZED                                        \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_NOT_INITIALIZED)
/*! Feature or function is not available. */
#define ADI_SPI_ERR_FEATURE_NOT_SUPPORTED                                  \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_FEATURE_NOT_SUPPORTED)
/*! Timeout occured. */
#define ADI_SPI_ERR_TIMEOUT                                                \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_TIMEOUT)
/*! The device is busy, try again later. */
#define ADI_SPI_ERR_DEVICE_BUSY                                            \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_DEVICE_BUSY)
/*! Invalid handle was passed. */
#define ADI_SPI_ERR_INVALID_HANDLE                                         \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_INVALID_HANDLE)
/*! Semaphore could not be created. */
#define ADI_SPI_ERR_SEMAPHORE_CREATE                                       \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_SEMAPHORE_CREATE)
/*! The driver's used version is not supported. */
#define ADI_SPI_ERR_UNSUPPORTED_VERSION                                    \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_UNSUPPORTED_VERSION)
/*! The driver's used version is not supported. */
#define ADI_SPI_ERR_FROM_DRIVER                                            \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_FROM_DRIVER)
/*! The device/handle is not open.*/
#define ADI_SPI_ERR_NOT_OPEN                                               \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_NOT_OPEN)
/*! The file is written failed. */
#define ADI_SPI_ERR_WRITE_FAILED                                           \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_WRITE_FAILED)
/*! The file is read failed. */
#define ADI_SPI_ERR_READ_FAILED                                            \
                              (ADI_SPI_MODULE_BASE + ADI_ERR_READ_FAILED)


/*
*******************************************************************************
** \brief Configuration parameters for SPI.
*******************************************************************************
*/
typedef struct
{
    /*Device node path*/
    ADI_CHAR *nodePath;
    /* Mode of SPI */
    ADI_U8    mode;
    /* Temporary override of the device's wordsize. */
    ADI_U8    bits;
    /* temporary override of the device's bitrate. */
    ADI_U32   speed;
    /*if lsb = 0, SPI transmit high bit first, or transmit low bit first.*/
    ADI_U8    lsb;
}ADI_SPI_OpenParam;


/*
*******************************************************************************
** \brief Configuration parameters for adi_spi_general_ioctl();.
*******************************************************************************
*/
typedef struct
{
    /*A pointer to data user want to send*/
    void *txBuf;
    /*A pointer to buffer where user want to save data*/
    void *rxBuf;
    /*Buffer size*/
    ADI_U32 bufLen;
}ADI_SPI_OpenParamTransfer;

//*****************************************************************************
//*****************************************************************************
//** API Functions
//*****************************************************************************
//*****************************************************************************

#ifdef __cplusplus
extern "C" {
#endif


/*!
*******************************************************************************
** \brief Open one SPI ADI instance.
**
** \param[in] ErrorCode: A pointer to return the error code.
**
** \param[in] UserConfig: Contains SPI configuration parameters.
**
** \return
**         - #(ADI_SYS_HandleT)spihd                  An valid handle of spi ADI instance
**                                                                          when function calls success.
**         - #NULL                                                      Error occurred.
** \sa
**         - adi_spi_close
**
*******************************************************************************
*/
ADI_SYS_HandleT adi_spi_open(ADI_ERR *errorCodePtr,
                                        ADI_SPI_OpenParam *userConfig);

/*!
*******************************************************************************
** \brief Close SPI ADI instance.
**
** \param[in] handle: Valid SPI ADI instance handle previously opened by #adi_spi_open.
**
** \return
**         - #ADI_OK                                                 On success.
**         - #ADI_SPI_ERR_BAD_PARAMETER                Invalid parameter.
**
** \sa
**         - adi_spi_open
**
*******************************************************************************
*/
ADI_ERR adi_spi_close(ADI_SYS_HandleT handle);

/*!
*******************************************************************************
** \brief Only send data by SPI, in half-duplex mode.
**
** \parame[in] handle: Valid SPI ADI instance handle previously opened by #adi_spi_open.
**
** \parame[in] data:  A pointer to data user want to send.
**
** \parame[in] dataLen: Length of data.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_SPI_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_SPI_ERR_READ_FAILED                       Data received failed.
**
** \sa
**         - adi_spi_write_only
**
*******************************************************************************
*/
ADI_ERR adi_spi_read_only(ADI_SYS_HandleT handle, void *data,
                                    ADI_U32 dataLen);



/*!
*******************************************************************************
** \brief Only receive data by SPI, in half-duplex mode.
**
** \parame[in] handle: Valid SPI ADI instance handle previously opened by #adi_spi_open.
**
** \parame[in] data:  A pointer to buffer user want to receive.
**
** \parame[in] dataLen: Length of buffer.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_SPI_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_SPI_ERR_WRITE_FAILED                     Data sent failed.
**
** \sa
**         - adi_spi_read_only
**
*******************************************************************************
*/
ADI_ERR adi_spi_write_only(ADI_SYS_HandleT handle, void *data,
                                    ADI_U32 dataLen);



/*!
*******************************************************************************
** \brief Send and receive data simultaneously, in full-duplex mode.
**
** \parame[in] handle: Valid SPI ADI instance handle previously opened by #adi_spi_open.
**
** \parame[in] txData:  A pointer to data user want to send.
**
** \parame[in] rxData:  A pointer to buffer user want to receive.
**
** \parame[in] dataLen:  Length of buffer.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_SPI_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_SPI_ERR_FROM_DRIVER                      Can't send spi message.
**
** \sa
**         - adi_spi_open
**
*******************************************************************************
*/
ADI_ERR adi_spi_write_and_read(ADI_SYS_HandleT handle, void *txData,
    void *rxData, ADI_U32 dataLen);


/*!
*******************************************************************************
** \brief Send data and then receive data, in half-duplex mode.
**
** \parame[in] handle: Valid SPI ADI instance handle previously opened by #adi_spi_open.
**
** \parame[in] txData:  A pointer to data user want to send.
**
** \parame[in] txdataLen:  Length of txdata.
**
** \parame[in] rxData:  A pointer to buffer user want to receive.
**
** \parame[in] rxdataLen:  Length of buffer.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_SPI_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_SPI_ERR_FROM_DRIVER                      Can't send spi message.
**
** \sa
**         - adi_spi_open
**
*******************************************************************************
*/
ADI_ERR adi_spi_write_then_read(ADI_SYS_HandleT handle, void *txData,
    ADI_U32 txDataLen, void *rxData, ADI_U32 rxDataLen);

/*!
*******************************************************************************
** \brief General SPI operation ADI instance,
**          half-duplex or full-duplex mode can be set by ADI_SPI_OpenParamTransfer .
**
** \parame[in] handle: Valid SPI ADI instance handle previously opened by #adi_spi_open.
**
** \parame[in] p:  A pointer to data structures user want to set.
**
** \parame[in] pNum:  Numbers of data structures.
**
** \return
**         - #ADI_OK                                                   On success.
**         - #ADI_SPI_ERR_BAD_PARAMETER                  Invalid parameter.
**         - #ADI_SPI_ERR_FROM_DRIVER                      Can't send spi message.
**
** \sa
**         - adi_spi_open
**
*******************************************************************************
*/
ADI_ERR adi_spi_general_ioctl(ADI_SYS_HandleT handle,
    ADI_SPI_OpenParamTransfer *p, ADI_U32 pNum);




#ifdef __cplusplus
    }
#endif



#endif /* _ADI_SPI_H_ */

/*!
*****************************************************************************
** \file           adi/src/adi_spi.c
**
** \brief          ADI spi module function
**
** \attention   THIS SAMPLE CODE IS PROVIDED AS IS. ---- MICROELECTRONICS
**                  ACCEPTS NO RESPONSIBILITY OR LIABILITY FOR ANY ERRORS OR
**                  OMMISSIONS
**
** (C) Copyright 2013-2014 by ---- MICROELECTRONICS CO.,LTD
**
*****************************************************************************
*/

#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <stdint.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/types.h>
#include <linux/spi/spidev.h>

#include "adi_types.h"
#include "adi_spi.h"

//*****************************************************************************
//*****************************************************************************
//** Local Defines
//*****************************************************************************
//*****************************************************************************
#define adi_sys_malloc malloc
#define adi_sys_memset memset
#define adi_sys_free free
#define ADI_ERROR printf

//*****************************************************************************
//*****************************************************************************
//** Local structures
//*****************************************************************************
//*****************************************************************************
typedef struct {
    /* file descriptor of device node. */
    ADI_S32 fdSpidev;
    /* mode of SPI */
    ADI_U8 mode;
    /* Temporary override of the device's wordsize. */
    ADI_U8 bits;
    /* temporary override of the device's bitrate. */
    ADI_U32 speed;
    /*if lsb = 0, SPI transmit high bit first, or transmit low bit first. */
    ADI_U8 lsb;
} ADI_SPI_HandleT;

//*****************************************************************************
//*****************************************************************************
//** Global Data
//*****************************************************************************
//*****************************************************************************

//*****************************************************************************
//*****************************************************************************
//** Local Data
//*****************************************************************************
//*****************************************************************************

//*****************************************************************************
//*****************************************************************************
//** Local Functions Declaration
//*****************************************************************************
//*****************************************************************************
static ADI_ERR spi_check_params(ADI_SPI_OpenParam * configPtr);
static ADI_ERR spi_set_config(ADI_SPI_HandleT * handlePtr,
    ADI_SPI_OpenParam * configPtr);


//*****************************************************************************
//*****************************************************************************
//** API Functions
//*****************************************************************************
//****************************************************************************
ADI_SYS_HandleT adi_spi_open(ADI_ERR * errorCodePtr,
    ADI_SPI_OpenParam * userConfig)
{

    ADI_SPI_HandleT *spiHandle = NULL;

    /* get memory for spiHandle */
    spiHandle = adi_sys_malloc(sizeof(ADI_SPI_HandleT));
    if (spiHandle == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        *errorCodePtr = ADI_SPI_ERR_OUT_OF_MEMORY;
        return NULL;
    }
    adi_sys_memset(spiHandle, 0, sizeof(ADI_SPI_HandleT));

    /* parameter checkout */
    *errorCodePtr = spi_check_params(userConfig);
    if (*errorCodePtr != ADI_OK) {
        ADI_ERROR("parameters error!\n");
        goto error_out;
    }
    /* get parameter for spiHandle and set */
    *errorCodePtr = spi_set_config(spiHandle, userConfig);
    if (*errorCodePtr != ADI_OK) {
        ADI_ERROR("set parameters failed!\n");
        goto error_out;
    }

    return (ADI_SYS_HandleT) spiHandle;

  error_out:
    adi_sys_free(spiHandle);
    spiHandle = NULL;
    return NULL;

}

ADI_ERR adi_spi_close(ADI_SYS_HandleT handle)
{

    ADI_SPI_HandleT *handleOperate = (ADI_SPI_HandleT *) handle;

    if (handleOperate == NULL) {
        return ADI_SPI_ERR_BAD_PARAMETER;
    }
    close(handleOperate->fdSpidev);
    adi_sys_free(handleOperate);
    handleOperate = NULL;
    return ADI_OK;

}

ADI_ERR adi_spi_read_only(ADI_SYS_HandleT handle, void *data,
    ADI_U32 dataLen)
{

    ADI_S32 ret = 0;
    void *rx = data;
    ADI_SPI_HandleT *handleOperate = (ADI_SPI_HandleT *) handle;

    if (handleOperate == NULL || rx == NULL || dataLen <= 0 || dataLen > 4096) {
        return ADI_SPI_ERR_BAD_PARAMETER;
    }
    ret = read(handleOperate->fdSpidev, rx, dataLen);
    if (ret < 0 || ret != dataLen) {
        ADI_ERROR("Spi read only failed.\n");
        return ADI_SPI_ERR_READ_FAILED;
    }

    return ADI_OK;

}

ADI_ERR adi_spi_write_only(ADI_SYS_HandleT handle, void *data,
    ADI_U32 dataLen)
{

    ADI_S32 ret = 0;
    void *tx = data;
    ADI_SPI_HandleT *handleOperate = (ADI_SPI_HandleT *) handle;

    if (handleOperate == NULL || tx == NULL || dataLen <= 0 || dataLen > 4096) {
        return ADI_SPI_ERR_BAD_PARAMETER;
    }
    ret = write(handleOperate->fdSpidev, tx, dataLen);
    if (ret < 0 || ret != dataLen) {
        ADI_ERROR("Spi write only failed\n");
        return ADI_SPI_ERR_WRITE_FAILED;
    }

    return ADI_OK;

}

ADI_ERR adi_spi_write_then_read(ADI_SYS_HandleT handle, void *txData,
    ADI_U32 txDataLen, void *rxData, ADI_U32 rxDataLen)
{

    ADI_S32 ret = 0;
    void *tx = txData;
    void *rx = rxData;
    ADI_SPI_HandleT *handleOperate = (ADI_SPI_HandleT *) handle;

    if (handleOperate == NULL || tx == NULL || txDataLen <= 0
        || rx == NULL || rxDataLen <= 0) {
        return ADI_SPI_ERR_BAD_PARAMETER;
    }
    struct spi_ioc_transfer tr[2] = {
        {
                .tx_buf = (unsigned long) tx,
                .rx_buf = (unsigned long) NULL,
                .len = txDataLen,
                .speed_hz = handleOperate->speed,
                .bits_per_word = handleOperate->bits,
            },
        {
                .tx_buf = (unsigned long) NULL,
                .rx_buf = (unsigned long) rx,
                .len = rxDataLen,
                .speed_hz = handleOperate->speed,
                .bits_per_word = handleOperate->bits,
            },
    };
    ret = ioctl(handleOperate->fdSpidev, SPI_IOC_MESSAGE(2), tr);
    if (ret <= 0) {
        ADI_ERROR("Spi write then read failed.\n");
        return ADI_SPI_ERR_FROM_DRIVER;
    }

    return ADI_OK;

}

ADI_ERR adi_spi_write_and_read(ADI_SYS_HandleT handle, void *txData,
    void *rxData, ADI_U32 dataLen)
{

    ADI_S32 ret = 0;
    void *tx = txData;
    void *rx = rxData;
    ADI_SPI_HandleT *handleOperate = (ADI_SPI_HandleT *) handle;

    if (handleOperate == NULL || tx == NULL || rx == NULL || dataLen <= 0) {
        return ADI_SPI_ERR_BAD_PARAMETER;
    }
    struct spi_ioc_transfer tr = {
        .tx_buf = (unsigned long) tx,
        .rx_buf = (unsigned long) rx,
        .len = dataLen,
        .speed_hz = handleOperate->speed,
        .bits_per_word = handleOperate->bits,
    };
    ret = ioctl(handleOperate->fdSpidev, SPI_IOC_MESSAGE(1), &tr);
    if (ret < 1) {
        ADI_ERROR("Spi write and read failed.\n");
        return ADI_SPI_ERR_FROM_DRIVER;
    }

    return ADI_OK;

}

ADI_ERR adi_spi_general_ioctl(ADI_SYS_HandleT handle,
    ADI_SPI_OpenParamTransfer * p, ADI_U32 pNum)
{

    ADI_S16 i;
    ADI_S32 ret = 0;
    ADI_SPI_OpenParamTransfer *pData = p;
    ADI_SPI_HandleT *handleOperate = (ADI_SPI_HandleT *) handle;

    if (handleOperate == NULL || pNum < 0 || pData == NULL) {
        return ADI_SPI_ERR_BAD_PARAMETER;
    }
    struct spi_ioc_transfer *tr =
        adi_sys_malloc(pNum * sizeof(struct spi_ioc_transfer));
    if (tr == NULL) {
        ADI_ERROR("allocate memory failed!\n");
        return ADI_SPI_ERR_OUT_OF_MEMORY;
    }
    for (i = 0; i < pNum; i++) {
        tr[i].tx_buf = (unsigned long) pData[i].txBuf;
        tr[i].rx_buf = (unsigned long) pData[i].rxBuf;
        tr[i].len = pData[i].bufLen;
        tr[i].speed_hz = handleOperate->speed;
        tr[i].bits_per_word = handleOperate->bits;
    }
    ret = ioctl(handleOperate->fdSpidev, SPI_IOC_MESSAGE(pNum), tr);
    if (ret < 1) {
        ADI_ERROR("Operate spi failed.\n");
        free(tr);
        return ADI_SPI_ERR_FROM_DRIVER;
    }

    free(tr);
    return ADI_OK;

}

//*****************************************************************************
//*****************************************************************************
//** Local Functions
//*****************************************************************************
//*****************************************************************************

ADI_ERR spi_check_params(ADI_SPI_OpenParam * configPtr)
{

    if (strcmp(configPtr->nodePath, "/dev/spidev0.0") &&
        strcmp(configPtr->nodePath, "/dev/spidev0.1") &&
        strcmp(configPtr->nodePath, "/dev/spidev0.2") &&
        strcmp(configPtr->nodePath, "/dev/spidev0.3") &&
        strcmp(configPtr->nodePath, "/dev/spidev1.0")) {
        ADI_ERROR("nodePath error.\n");
        goto error_out;
    }

    if (configPtr->mode < 0 || configPtr->mode > 3) {
        ADI_ERROR("mode error.\n");
        goto error_out;
    }
    if (configPtr->bits < 4 || configPtr->bits > 16) {
        ADI_ERROR("bits error.\n");
        goto error_out;
    }
    if (configPtr->speed < 10000 || configPtr->speed > 10000000) {
        ADI_ERROR("speed error.\n");
        goto error_out;
    }
    if (configPtr->lsb < 0) {
        ADI_ERROR("lsb error.\n");
        goto error_out;
    }

    return ADI_OK;

  error_out:
    return ADI_SPI_ERR_BAD_PARAMETER;

}

ADI_ERR spi_set_config(ADI_SPI_HandleT * handlePtr,
    ADI_SPI_OpenParam * configPtr)
{

    ADI_S32 ret;

    ADI_U8 rdMode;
    ADI_U8 rdBits;
    ADI_U32 rdSpeed;
    ADI_U8 rdLsb;

    /* get parameter for handlePtr */
    handlePtr->mode = configPtr->mode;
    handlePtr->bits = configPtr->bits;
    handlePtr->speed = configPtr->speed;
    handlePtr->lsb = configPtr->lsb;

    /* open device node */
    handlePtr->fdSpidev = open(configPtr->nodePath, O_RDWR);
    if (handlePtr->fdSpidev < 0) {
        ADI_ERROR("Open %s failed!\n", configPtr->nodePath);
        goto error_out;
    }
    /* set SPI mode */
    ret = ioctl(handlePtr->fdSpidev, SPI_IOC_WR_MODE, &(handlePtr->mode));
    if (ret < 0) {
        ADI_ERROR("Set mode failed!.\n");
        goto error_out;
    }
    ret = ioctl(handlePtr->fdSpidev, SPI_IOC_RD_MODE, &rdMode);
    if (ret < 0) {
        ADI_ERROR("Read mode failed!\n");
        goto error_out;
    } else {
        if (rdMode != handlePtr->mode) {
            ADI_ERROR("Set mode faild!\n");
            goto error_out;
        }
    }
    /* set bits_per_word */
    ret =
        ioctl(handlePtr->fdSpidev, SPI_IOC_WR_BITS_PER_WORD,
        &(handlePtr->bits));
    if (ret < 0) {
        ADI_ERROR("Set bits faild!\n");
        goto error_out;
    }
    ret = ioctl(handlePtr->fdSpidev, SPI_IOC_RD_BITS_PER_WORD, &(rdBits));
    if (ret < 0) {
        ADI_ERROR("Read bits failed!\n");
        goto error_out;
    } else {
        if (rdBits != handlePtr->bits) {
            ADI_ERROR("Set bits faild!\n");
            goto error_out;
        }
    }
    /*set max speed */
    ret =
        ioctl(handlePtr->fdSpidev, SPI_IOC_WR_MAX_SPEED_HZ,
        &(handlePtr->speed));
    if (ret < 0) {
        ADI_ERROR("Set speed failed!\n");
        goto error_out;
    }
    ret = ioctl(handlePtr->fdSpidev, SPI_IOC_RD_MAX_SPEED_HZ, &(rdSpeed));
    if (ret < 0) {
        ADI_ERROR("Read speed failed!\n");
        goto error_out;
    } else {
        if (rdSpeed != handlePtr->speed) {
            ADI_ERROR("Set speed faild!\n");
            goto error_out;
        }
    }
    /*set lsb */
    ret = ioctl(handlePtr->fdSpidev, SPI_IOC_WR_LSB_FIRST, &(handlePtr->lsb));
    if (ret < 0) {
        ADI_ERROR("Set lsb failed!\n");
        goto error_out;
    }
    ret = ioctl(handlePtr->fdSpidev, SPI_IOC_RD_LSB_FIRST, &(rdLsb));
    if (ret < 0) {
        ADI_ERROR("Read lsb failed!\n");
        goto error_out;
    } else {
        if (rdLsb != handlePtr->lsb) {
            ADI_ERROR("Set lsb faild!\n");
            goto error_out;
        }
    }

    return ADI_OK;

  error_out:
    return ADI_SPI_ERR_FROM_DRIVER;

}

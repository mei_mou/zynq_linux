/**************************************************************************
  * @brief        : linux  spi测试例程
  * @author       : shiweisun@foxmail.com
  * @copyright    : NONE
  * @version      : 0.1
  * @note         :   
  * @history      : 
***************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <sys/epoll.h>

#include "adi_types.h"
//#include "adi_sys.h"
#include "adi_spi.h"

int usr_spi_init(ADI_SYS_HandleT *hd,ADI_CHAR *dev)
{
    ADI_ERR errorCode = ADI_OK;    
    ADI_SPI_OpenParam config;
    config.nodePath = dev;
    config.mode     = 0;
    config.bits     = 8;
    config.speed    = 400000;
    config.lsb      = 0;
    *hd = adi_spi_open(&errorCode, &config);
    if (errorCode != ADI_OK)
    {
        printf("adi_spi_open() failed.\n");
        return errorCode;
    }
    return 0;
}

int 
usr_spi_exit(ADI_SYS_HandleT *hd)
{
    ADI_ERR errorCode = ADI_OK;
    if(*hd)
    {
        errorCode = adi_spi_close(*hd);
        if (errorCode != ADI_OK)
        {
            printf("adi_spi_close() failed.\n");
            return errorCode;
        }
        else
        {
            printf("spi closed.\n");
            *hd = NULL;
        }
    }
    return 0;
}

int 
usr_spi_write_or_read(ADI_SYS_HandleT *hd,void* write_data, ADI_U32 write_len,
                            void* read_data, ADI_U32 read_len)
{
    ADI_ERR ret = ADI_OK;
    if(NULL == write_data || write_len == 0)
    {
        printf("parame error.\n");
        return -1;
    }
    ret = adi_spi_write_only(*hd, write_data, write_len);
    if (ret != ADI_OK)
    {
        printf("adi_spi_write_only() failed.\n");
        return ret;
    }
    if(read_data && read_len)
    {
        ret = adi_spi_read_only(*hd, read_data, read_len);
        if (ret != ADI_OK )
        {
            printf("adi_spi_write_and_read() failed.\n");
            return ret;
        }
    }
    return ret;
}

int 
usr_spi_write(ADI_SYS_HandleT *hd,void * write_data, ADI_U32 write_len)
{
    ADI_ERR ret = ADI_OK;
    printf("%s\n",__func__);
    ret = adi_spi_write_only(*hd, write_data, write_len);
    if(ADI_SPI_ERR_BAD_PARAMETER == ret)
    {
        printf("%s parame error.\n",__func__);
        return -1;
    }    
    else if (ret != ADI_OK)
    {
        printf("%s failed.\n",__func__);
        return ret;
    }
    return write_len;
}
int 
usr_spi_read(ADI_SYS_HandleT *hd,void * rd_buf, ADI_U32 rd_len)
{
    ADI_ERR ret = ADI_OK;
    printf("%s\n",__func__);
    ret = adi_spi_read_only(*hd, rd_buf, rd_len);
    if(ADI_SPI_ERR_BAD_PARAMETER == ret)
    {
        printf("%s parame error.\n",__func__);
        return -1;
    }    
    else if (ret != ADI_OK)
    {
        printf("%s failed.\n",__func__);
        return ret;
    }
    return rd_len;
}

int 
usr_spi_write_then_read(ADI_SYS_HandleT *hd,void * write_data,       ADI_U32 write_len,
                                void * rd_buf, ADI_U32 rd_len)
{
    ADI_ERR ret = ADI_OK;
    printf("%s\n",__func__);
    ret = adi_spi_write_then_read(*hd, write_data,write_len,rd_buf, rd_len);
    if(ADI_SPI_ERR_BAD_PARAMETER == ret)
    {
        printf("%s parame error.\n",__func__);
        return -1;
    }    
    else if (ret != ADI_OK)
    {
        printf("%s failed.\n",__func__);
        return ret;
    }
    return 0;
}


#define SPIDEV "/dev/spidev1.0"

int main(int argc, char * argv[])
{
	int i ;
	int cnt;
	char wr_buf[64]; 
	char rd_buf[64];

    
    static ADI_SYS_HandleT spihd;
	printf("%s, build time:%s-%s\n",__FILE__,__DATE__,__TIME__);
	
	if(0 != usr_spi_init(&spihd,SPIDEV))
	{
	    printf("device init erro!\n");
	}

	/*init the buffer*/
	for(i=0;i<sizeof(wr_buf);i++)
	{
		wr_buf[i]=0x55+i;
	}
	memset(rd_buf,0,sizeof(rd_buf));
	
	cnt = 10;
	while(cnt--)
	{
		usr_spi_write(&spihd,(void * )&wr_buf,sizeof(wr_buf));
		sleep(1);
		usr_spi_read(&spihd,(void * )&rd_buf,sizeof(rd_buf));
		sleep(1);
		usr_spi_write_then_read(&spihd,(void * )&wr_buf,sizeof(wr_buf),
		        (void * )&rd_buf,sizeof(rd_buf));
		sleep(1);
	}
	
    usr_spi_exit(&spihd);
	return 0;
}

#!/bin/bash -e
###############################################################################
#
# created by cavin <shiweisun@foxmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
###############################################################################
echo -e "\033[32mexport sdk command \033[0m"
source ${ZYNQ_SCRIPT_DIR}/fun_local.sh


# create project
function create_project(){
    echo -e "\033[32mFun: Create project "$1"...\033[0m";
    
    PROJECT_NAME=$1
    is_dir_exist ${ZYNQ_WORKSPACE} ${PROJECT_NAME}
    if [ $? = 0 ]; then
        echo -e "\033[31m[ERROR]Project dir:"$1" had exist!\033[0m";
        save_project_name ${ZYNQ_WORKSPACE} ${PROJECT_NAME}
        
        return 1
    fi
    mkdir -p ${ZYNQ_WORKSPACE}/${PROJECT_NAME}
    PROJECT_DIR=${ZYNQ_WORKSPACE}/${PROJECT_NAME}
    SAMPLE_PROJECT_DIR=${ZYNQ_WORKSPACE}/hw_ax7010_linuxbase
    cp -rf ${SAMPLE_PROJECT_DIR}/configs  ${PROJECT_DIR}
    mkdir -p ${PROJECT_DIR}/images  ${PROJECT_DIR}/hw_src   ${PROJECT_DIR}/temp_dir
    cp -f  ${SAMPLE_PROJECT_DIR}/images/.gitignore   ${PROJECT_DIR}/images 
    cp -f  ${SAMPLE_PROJECT_DIR}/hw_src/.gitignore   ${PROJECT_DIR}/hw_src
    cp -f  ${SAMPLE_PROJECT_DIR}/images/.gitignore   ${PROJECT_DIR}/temp_dir
    cp -rf  ${SAMPLE_PROJECT_DIR}/fs   ${PROJECT_DIR}/ 
    echo ${PROJECT_DIR}
    save_project_name ${ZYNQ_WORKSPACE} ${PROJECT_NAME}
    export ZYNQ_TARGET_DIR=${PROJECT_DIR}/images 
    return 0
}

# build kernel 
sdk_create_project() {
    if [ -n "$1" ] ;then
        #echo -e "\033[32m Create project "$1"...\033[0m";      
        create_project $1 
        if [ $? -ne 0 ] ; then
            echo -e "\033[31m[ERROR]Create project "$1" fail!\033[0m"; 
        else
            echo -e "\033[32mCreate project "$1" successed.\033[0m";
        fi
    else
        echo -e "\033[31m[ERROR]You need input the name of project!!!\033[0m";
    fi    
    cur_project_check #检查当前工程是否有设置
    if [ $? = 0 ] ; then
        export ZYNQ_TARGET_DIR=${ZYNQ_WORKSPACE}/${PROJECT_NAME}/images 
    fi
}
export -f sdk_create_project
# build kernel 
sdk_build_kernel() {
    #argc=$#
    #echo ${argc}
    if [ -n "$1" ] ;then
        if test [ "$1" = "-a" ] ;then
            echo -e "\033[32mrebuild kernel all \033[0m";
            build_kernel_all
        else
            echo -e "\033[31m[ERROR]  $1 invalid!\033[0m";
        fi
    else
        echo -e "\033[32mbuild kernel \033[0m";
        build_kernel
    fi    
}
export -f sdk_build_kernel

#build uboot
sdk_build_uboot() {
    if [ -n "$1" ] ;then
        if test [ "$1" = "-a" ] ;then
            echo -e "\033[32mrebuild uboot all \033[0m";
            build_uboot_all
        else
            echo -e "\033[31m[ERROR]  $1 invalid!\033[0m";
        fi
    else
        echo -e "\033[32mbuild uboot \033[0m";
        build_uboot
    fi  
}
export -f sdk_build_uboot

sdk_build_bootbin(){
    if [ -n "$1" ] ;then
        if test [ "$1" = "-a" ] ;then
            echo -e "\033[32mGenerate BIN include all image \033[0m";
            bootbin_gen_all
        else
            echo -e "\033[31m[ERROR]  $1 invalid!\033[0m";
        fi
    else
        echo -e "\033[32mGenerate BIN only include fsbl and uboot \033[0m";
        bootbin_gen
    fi  
}
export -f sdk_build_bootbin

build_fs_usage() {
    echo -e "\033[31m[ERROR]  command is invalid!\033[0m";
    echo "e.g.:"
    echo "sdk_build_fs -create -rootfs          #创建rootfs文件"
    echo "sdk_build_fs -create -datafs          #创建datafs文件,实际是一个空目录"
    echo "sdk_build_fs -package -ramdisk [PATH] #将[PATH]目录打包成ramdisk"
    echo "sdk_build_fs -package -jffs2 [PATH]   #将[PATH]目录打包成jffs2"
    echo "sdk_build_fs -package -datafs [PATH]  #将datafs目录打包成jffs2格式"
}
sdk_build_fs(){
    if [ -n "$1" ] ;then
        case $1 in
        "-create") 
            if [ -n "$2" ] ;then
                case $2 in
                "-rootfs") 
                    echo -e "\033[32mCreate rootfs \033[0m";
                    fs_create_rootfs
                ;;
                
                "-datafs") 
                    echo -e "\033[32mCreate datafs\033[0m";
                    fs_create_datafs
                ;;
                
                *) build_fs_usage
                ;;
                esac
            else
                build_fs_usage
            fi  
        ;;
        
        "-package") 
            if [ -n "$2" ] ;then
                case $2 in
                "-ramdisk") 
                    echo -e "\033[32mPackage ramdisk fs \033[0m";
                    fs_package_ramdisk
                ;;
                
                "-datafs") 
                    echo -e "\033[32mPackage datafs\033[0m";
                    fs_package_datafs
                ;;
                
                "-jffs2") 
                    echo -e "\033[31mSorry, this feature is not supported for the time being.\033[0m";
                    
                ;;
                
                *) build_fs_usage
                ;;
                esac
            else
                echo -e "\033[31m[ERROR]  command is invalid!\033[0m";
                build_fs_usage
            fi  
        ;;
        
        *) build_fs_usage
        ;;
        esac
    else
        build_fs_usage
    fi 
    
    
}
export -f sdk_build_fs

sdk_build_dtb(){
    echo -e "\033[32mbuild device tree \033[0m";
    build_dtb   
}
export -f sdk_build_dtb

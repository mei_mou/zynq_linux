#!/bin/bash -e
###############################################################################
#
# created by cavin <shiweisun@foxmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
###############################################################################
#echo -e "\033[32mfun local \033[0m"
function is_dir_exist(){
    path=$1 
    dir=$2
    #echo -e "\033[32mFun: is_dir_exist "$1"/"$2"\033[0m";
    for  result in $(ls -A ${path} | grep ${dir})
        do
            if [ "${result}" = "${dir}" ]; then
                #echo -e "\033[32mFun: check dir:${dir} ok\033[0m";
                return 0            
            fi
        done
    #echo -e "\033[31m[ERROR] check dir:${dir} fail \033[0m";
    return 1
}

function is_file_exist(){
    path=$1 
    file=$2
    #echo -e "\033[32mFun: is_file_exist "$1"/"$2"\033[0m";
    for  result in $(ls -A ${path} | grep ${file})
        do
            if [ "${result}" = "${file}" ]; then
                #echo -e "\033[32mFun: check file:${file} ok\033[0m";
                return 0            
            fi
        done
    #echo -e "\033[31m[ERROR] check file:${file} fail \033[0m";
    return 1
}
function is_file_exist_fuzzy(){
    path=$1 
    file=$2
    if [ "`ls -A ${path} | grep ${file}`" = "" ]; then
        return 1
    else
        return 0
    fi
}
function is_dir_exist_fuzzy(){
    path=$1 
    dir=$2
    if [ "`ls -A ${path} | grep ${dir}`" = "" ]; then
        return 1
    else
        return 0
    fi
}

function save_project_name(){
echo -e "\033[32mFun: save_project_name "$1"...\033[0m";
    path=$1 
    name=$2 
    echo ${path} ${name}
cat>${path}/cur_project_name<<EOF
${name}
EOF
    return 0
}

function cur_project_check(){
    cur_project_file=${ZYNQ_WORKSPACE}/cur_project_name
    is_file_exist "$(dirname ${cur_project_file})" "$(basename ${cur_project_file})"
    if [ $? = 0 ]; then
        line_num="`wc -l ${cur_project_file}`" #统计文件行数
        line_num=${line_num%%' '*} #从左往右截取空格之前的字符
        if [ ${line_num} != 1 ] ; then
            echo_error "the file:${cur_project_file} is invalid!"
            echo_warn "You need create project or rewrite the file: ${cur_project_file}!"
            echo file line:${line_num}
            return 1
        else
            PROJECT_NAME="`cat ${cur_project_file}`"
            is_dir_exist ${ZYNQ_WORKSPACE} ${PROJECT_NAME}
            if [ $? = 1 ] ; then
                echo_error "Project dir:${PROJECT_NAME} didn't exist!";
                echo_warn "Project did not exist,you need create project!";
                return 1
            else
                echo 
                echo -e "\033[32mCurent project is:${PROJECT_NAME}\033[0m";            
                export ZYNQ_CUR_PROJECT=${PROJECT_NAME}
                export ZYNQ_CUR_PROJECT_DIR=${ZYNQ_WORKSPACE}/${PROJECT_NAME}
                echo  ZYNQ_CUR_PROJECT_DIR:${ZYNQ_WORKSPACE}/${PROJECT_NAME}
                return 0
            fi
        fi
    else
        echo_error "Curent project is not exist"
        echo_warn "You need create project or rewrite the file: ${cur_project_file}!"
        return 1
    fi
}

function build_kernel(){
    if [ ! "${ZYNQ_ENV_CFG_OK}" ];then
        echo "\033[31m[ERROR]Please source the env_config first!\033[0m \n"
        return 1
    fi
    echo -e "\033[33m kernel building...! \033[0m"
    cd ${ZYNQ_KERNEL_DIR}/
    make uImage LOADADDR=0x00008000
    
    if [ "${ZYNQ_TARGET_DIR}" != "" ] ; then
        cp -f ${ZYNQ_KERNEL_DIR}/arch/arm/boot/uImage  ${ZYNQ_TARGET_DIR}/
    fi
    echo -e "\033[32m kernel build finished!\033[0m"
    cd -
}

function build_kernel_all(){
    if [ ! "${ZYNQ_ENV_CFG_OK}" ];then
        echo "\033[31m[ERROR]Please source the env_config first!\033[0m \n"
        return 1
    fi
    USR_CFG_DIR=${ZYNQ_CUR_PROJECT_DIR}/configs
    USR_HW_DIR=${ZYNQ_CUR_PROJECT_DIR}/hw_src
    echo -e "\033[32m kernel build  prepare...! \033[0m"
    cp -f ${USR_CFG_DIR}/kernel/zynq_usr_kernel_defconfig  ${ZYNQ_KERNEL_DIR}/arch/arm/configs/
    
    cd ${ZYNQ_KERNEL_DIR}/    
    make distclean   #第二次编译时可以将这里注释掉，加快编译速度
    make zynq_usr_kernel_defconfig #第二次编译时可以将这里注释掉，加快编译速度
    cd -
    build_kernel
}

function bin_generate(){
    USR_HW_DIR=${ZYNQ_CUR_PROJECT_DIR}/hw_src
    is_file_exist_fuzzy ${USR_HW_DIR} fsbl
    if [ $? != 0 ]; then
        echo -e "\033[31m[ERROR]The fsbl  didn't exist in ${USR_HW_DIR}!\033[0m"; 
        echo -e "\033[31m[ERROR]工程下没有fsbl文件，请用Xilinx SDK工具生成 \033[0m"; 
        return 1
    fi
    FSBL=${USR_HW_DIR}/$(ls ${USR_HW_DIR}/ | grep fsbl )
    FPGA_BIT=${USR_HW_DIR}/$(ls ${USR_HW_DIR}/ | grep .bit )
    cp -f ${ZYNQ_SCRIPT_DIR}/bootgen/${BIF_SRC}  ${ZYNQ_TARGET_DIR}/output.bif
    cp -f ${FSBL}   ${ZYNQ_TARGET_DIR}/zynq_fsbl.elf
    cp -f ${FPGA_BIT}   ${ZYNQ_TARGET_DIR}/system.bit
    cd ${ZYNQ_TARGET_DIR}
    ${ZYNQ_TOOLS_DIR}/others/bootgen_201803/bin/bootgen -arch zynq -image  output.bif -o i ${TARGET} -w on
    if [ $? = 0 ] ; then
        echo -e "\033[32m ${TARGET} generated!\033[0m"
    else
        echo -e "\033[31m ${TARGET} generate Fail!\033[0m"
    fi
    rm -f ${ZYNQ_TARGET_DIR}/output.bif ${ZYNQ_TARGET_DIR}/zynq_fsbl.elf
    cd -
}

function bootbin_gen(){    
    BIF_SRC=output.bif 
    TARGET=BOOT.BIN
    bin_generate
}

function bootbin_gen_all(){
    
    BIF_SRC=output_all_parts.bif 
    TARGET=ALL_IMAGES.BIN
    bin_generate
    echo -e "\033[32m All end!\033[0m"
}

function build_uboot(){
    if [ ! "${ZYNQ_ENV_CFG_OK}" ];then
        echo -e "\033[31m[ERROR]Please source the env_config first!\033[0m \n"
        return 1
    fi
    cd ${ZYNQ_UBOOT_DIR}/
    echo -e "\033[33m uboot building...! \033[0m"
    make  
    if [ "${ZYNQ_TARGET_DIR}" != "" ] ; then
        cp -f ${ZYNQ_UBOOT_DIR}/u-boot  ${ZYNQ_TARGET_DIR}/u-boot.elf
    fi
    echo -e "\033[32m uboot build finished!\033[0m"
    cd -
}

function build_uboot_all(){
    if [ ! "${ZYNQ_ENV_CFG_OK}" ];then
        echo -e "\033[31m[ERROR]Please source the env_config first!\033[0m \n"
        return 1
    fi
    USR_CFG_DIR=${ZYNQ_CUR_PROJECT_DIR}/configs
    USR_HW_DIR=${ZYNQ_CUR_PROJECT_DIR}/hw_src
    echo -e "\033[32m uboot build  prepare...! \033[0m"
    cp -f ${USR_CFG_DIR}/uboot/zynq_usr_uboot.dts ${ZYNQ_UBOOT_DIR}/arch/arm/dts/
    cp -f ${USR_CFG_DIR}/uboot/zynq_usr_uboot.h  ${ZYNQ_UBOOT_DIR}/include/configs/
    cp -f ${USR_CFG_DIR}/uboot/zynq_usr_uboot_defconfig  ${ZYNQ_UBOOT_DIR}/configs/
    cd ${ZYNQ_UBOOT_DIR}/
    
    make distclean   #第二次编译时可以将这里注释掉，加快编译速度
    make zynq_usr_uboot_defconfig #第二次编译时可以将这里注释掉，加快编译速度
    cd -
    build_uboot


}

function fs_create_rootfs(){
    if [ ! "${ZYNQ_ENV_CFG_OK}" ];then
        echo -e "\033[31m[ERROR]Please source the env_config first!\033[0m \n"
        return 1
    fi
    
    is_dir_exist_fuzzy ${ZYNQ_PACKAGES_DIR}/rootfs  rootfs_ori_mini
    if [ $? != 0 ] ; then
        echo -e "\033[31m[ERROR]rootfs_ori_mini package didn't exist!\033[0m \n"
        return 1
    fi
    mkdir -p ${ZYNQ_FS_DIR}/rootfs
    for pkg_src in $(ls -A ${ZYNQ_PACKAGES_DIR}/rootfs | grep rootfs_ori_mini) 
    do
        sudo tar -xzf ${ZYNQ_PACKAGES_DIR}/rootfs/${pkg_src} -C ${ZYNQ_FS_DIR}/
        break;
    done    
    echo -e "\033[32mCreate rootfs finished!\033[0m"    
}

function fs_create_datafs(){
    if [ ! "${ZYNQ_ENV_CFG_OK}" ];then
        echo "\033[31m[ERROR]Please source the env_config first!\033[0m \n"
        return 1
    fi
    
    mkdir -p ${ZYNQ_FS_DIR}/datafs
    echo -e "\033[32mCreate datafs finished!\033[0m"    
}

function fs_package_ramdisk(){
    if [ ! "${ZYNQ_ENV_CFG_OK}" ];then
        echo -e "\033[31m[ERROR]Please source the env_config first!\033[0m \n"
        return 1
    fi
    
    fs_src=${ZYNQ_FS_DIR}/rootfs
    FS_MOUNT_DIR=${ZYNQ_FS_DIR}/mount_dir
    FS_TARGET=uramdisk.image.gz
    DISK_NAME=ramdisk
    FS_SIZE=20480
    cd ${ZYNQ_FS_DIR}/
    #创建一个空的ramdisk文件
    dd if=/dev/zero of=${DISK_NAME}.image bs=1024 count=${FS_SIZE}
    mke2fs -F ${DISK_NAME}.image -L "ramdisk" -b 1024 -m 0
    tune2fs ${DISK_NAME}.image -i 0
    chmod a+rwx ${DISK_NAME}.image

    echo -e "\033[32mCreate ${DISK_NAME}.image succeed! \033[0m" 
    #创建ramdisk目录，将ramdisk16M.image 挂载到该目录下
    mkdir ${FS_MOUNT_DIR}
    sudo mount -o loop ${DISK_NAME}.image ${FS_MOUNT_DIR}/
    #接下来，只需要将_rootfs目录下的内容全部拷贝到ramdisk下即可。
    sudo cp -R ${fs_src}/*  ${FS_MOUNT_DIR}
    if [ $? -eq 0 ]; then
        echo "copy rootfs succeed!"
    else
        echo_error "copy rootfs fail!"
        return 1
    fi
    #添加自己 配置文件
    usr_fs_dir=${ZYNQ_CUR_PROJECT_DIR}/fs
    source  ${usr_fs_dir}/usr_cfg/sh_fs_usr
    sudo umount ${FS_MOUNT_DIR}/
   
    echo "gzip the image..."
    gzip -9 ${DISK_NAME}.image
    ${ZYNQ_TOOLS_DIR}/others/mkimage/mkimage -n 'uboot ext2 ramdisk' -A arm -O linux -T ramdisk -C gzip  -d ${DISK_NAME}.image.gz  ${FS_TARGET}

    
    if [ "${ZYNQ_TARGET_DIR}" != "" ] ; then
        cp -f ${ZYNQ_FS_DIR}/${FS_TARGET}  ${ZYNQ_TARGET_DIR}/ 
    fi
    
    rm -rf ${FS_MOUNT_DIR}
    rm -rf ${DISK_NAME}.image.gz ${FS_TARGET}
    echo -e "\033[32mPackage ramdisk finished!\033[0m"   
    cd -
}

function fs_package_datafs(){
    if [ ! "${ZYNQ_ENV_CFG_OK}" ];then
        echo -e "\033[31m[ERROR]Please source the env_config first!\033[0m \n"
        return 1
    fi    
    datafs_src=${ZYNQ_FS_DIR}/datafs    
    FS_TARGET=data_jffs2.img
    PAGESIZE=0x1000 #4K
    BLOCKSIZE=0X1000 #4K
    FS_SIZE=0X200000 #2MByte
    cd ${ZYNQ_FS_DIR}/
    
    is_dir_exist ${ZYNQ_FS_DIR} datafs
    if [ $? != 0 ] ; then
        echo -e "\033[31m[ERROR]datafs direction didn't exist!\033[0m \n"
        return 1
    fi
    chmod +x ${ZYNQ_TOOLS_DIR}/others/mkimage/mkfs.jffs2
    ${ZYNQ_TOOLS_DIR}/others/mkimage/mkfs.jffs2 -e ${BLOCKSIZE} -l -s ${PAGESIZE} -p --pad=${FS_SIZE} -n -r ${datafs_src}  -o ./${FS_TARGET}
    if [ "${ZYNQ_TARGET_DIR}" != "" ] ; then
        cp -f ${ZYNQ_FS_DIR}/${FS_TARGET}  ${ZYNQ_TARGET_DIR}/ 
    fi
    rm -rf ${FS_TARGET}
    echo -e "\033[32mPackage datafs finished!\033[0m" 
    cd -    
}

function build_dtb(){
    echo -e "\033[32mbuild_dtb!\033[0m" 
    tmp_dir=${ZYNQ_CUR_PROJECT_DIR}/temp_dir
    dt_dir=${tmp_dir}/device_tree
    mkdir -p ${dt_dir}
    is_dir_exist_fuzzy ${ZYNQ_CUR_PROJECT_DIR}/hw_src device_tree
    if [ $? != 0 ] ; then
        echo -e "\033[31m[ERROR]device_tree direction didn't exist!\033[0m \n"
        return 1
    fi
    is_file_exist ${ZYNQ_CUR_PROJECT_DIR}/hw_src system-conf.dtsi
    if [ $? != 0 ] ; then
        echo -e "\033[31m[ERROR]system-conf.dtsi didn't exist!\033[0m \n"
        return 1
    fi
    
    for dt_src in $(ls -A ${ZYNQ_CUR_PROJECT_DIR}/hw_src | grep device_tree) 
    do
        cp -R -f ${ZYNQ_CUR_PROJECT_DIR}/hw_src/${dt_src}/*  ${dt_dir}/
        break;
    done  
    cp -f ${ZYNQ_CUR_PROJECT_DIR}/hw_src/system-conf.dtsi  ${dt_dir}/system-conf.dtsi
    
    cd ${dt_dir}
    for dt_top_ori in $(ls -A ${dt_dir} | grep top.dts) 
    do
        break;
    done 
    dt_top=dttmp.dts
    rm -f ${dt_top} >/dev/null 2>&1
    cp -f ${dt_top_ori} ${dt_top} 
    sed -i 's/#include/\/include\//g' ${dt_top}
    sed -i 's/memory {/memory@0 {/g' ${dt_top}
    sed -i "s/serial0\ =\ \&uart0/serial0\ =\ \&uart1/g" ${dt_top}
    sed -i "s/serial1\ =\ \&uart1/serial1\ =\ \&uart0/g" ${dt_top}
    echo '/include/ "system-conf.dtsi"'  >>  ${dt_top}
    if [ "${ZYNQ_TARGET_DIR}" != "" ] ; then
    #dtc编译
    ${ZYNQ_TOOLS_DIR}/others/dtc/dtc  -I dts -O dtb -o devicetree.dtb ${dt_top} 
    #dtb反编译成dts
    ${ZYNQ_TOOLS_DIR}/others/dtc/dtc -I dtb -O dts devicetree.dtb -o ${ZYNQ_TARGET_DIR}/devicetree.dts
    cp -f devicetree.dtb  ${ZYNQ_TARGET_DIR}/
    fi
    cd -
}
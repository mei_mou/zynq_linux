#!/bin/bash -e
###############################################################################
#
# Copyright (C) 2014 - 2018 by Yujiang Lin <lynyujiang@gmail.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# Bash scripting cheatsheet
# https://devhints.io/bash
#
# Bash Shortcuts For Maximum Productivity
# https://www.skorks.com/2009/09/bash-shortcuts-for-maximum-productivity/
#
###############################################################################

# echo Warning Message 
echo_warn() {
    echo -e "\033[33m[WARNING] $1\033[0m ";
}
export -f echo_warn

#echo Infomation Message
echo_info() {
    echo  -e "\033[32m[INFO] $1\033[0m ";
}
export -f echo_info

# => Writing a Error Message to the Console Window
echo_error() {
    echo -e "\033[31m[ERROR] $1\033[0m ";
}
export -f echo_error

# => Writing a Warning Message to the Console Window
print_warn() {
    printf "\033[33m$1\033[0m";
}
export -f print_warn

# => Writing a Infomation Message to the Console Window
print_info() {
    printf "\033[32m$1\033[0m";
}
export -f print_info

# => Writing a Error Message to the Console Window
print_error() {
    printf "\033[31m$1\033[0m";
}
export -f print_error

# => Writing a Error Message to the Console Window and exit
error_exit() {
    echo -e "\033[31m[ERROR] \033[0m $1";
    exit 1;
}
export -f error_exit

# => Check the script is being run by root user
check_root() {
    if [ `whoami` != root ]; then
        error_exit "$0 must be run as sudo user or root!"
    fi
}
export -f check_root

# => When the current user isn't root, re-exec the script through sudo.
run_as_root() {
    [ "$(whoami)" != "root" ] && exec sudo -- "$0" "$@"
}
export -f run_as_root

# => This function will return the code name of the Linux host release to the caller
get_host_type() {
    local  __host_type=$1
    local  the_host=`lsb_release -a 2>/dev/null | grep Codename: | awk {'print $2'}`
    eval $__host_type="'$the_host'"
}
export -f get_host_type

# => This function returns the version of the Linux host to the caller
get_host_version() {
    local  __host_ver=$1
    local  the_version=`lsb_release -a 2>/dev/null | grep Release: | awk {'print $2'}`
    eval $__host_ver="'$the_version'"
}
export -f get_host_version

# => This function returns the major version of the Linux host to the caller
# If the host is version 12.04 then this function will return 12
get_major_host_version() {
    local  __host_ver=$1
    get_host_version major_version
    eval $__host_ver="'${major_version%%.*}'"
}
export -f get_major_host_version

# => This function returns the minor version of the Linux host to the caller
# If the host is version 12.04 then this function will return 04
get_minor_host_version() {
    local  __host_ver=$1
    get_host_version minor_version
    eval $__host_ver="'${minor_version##*.}'"
}
export -f get_minor_host_version

setup_putty() {
    if ! which putty > /dev/null; then
        print_info "Installing putty"
        sudo apt-get install -y -qq putty putty-doc && echo "All Done!"

        print_info "Start putty by typing the following: "
        echo "putty"
    fi
}
export -f setup_putty

setup_screen() {
    if ! which screen > /dev/null; then
        print_info "Installing screen"
        sudo apt-get install -y -qq screen && echo "All Done!"

        print_info "Start screen by typing the following: "
        echo "screen /dev/ttyUSB0 115200 -T xterm"
    fi
}
export -f setup_screen

setup_minicom() {
    if ! which minicom > /dev/null; then
        print_info "Installing minicom"
        sudo apt-get install -y -qq minicom && echo "All Done!"

        print_info "Start minicom by typing the following: "
        echo "minicom -b 115200 -D /dev/ttyUSB0"
    fi
}
export -f setup_minicom

setup_picocom() {
    if ! which picocom > /dev/null; then
        print_info "Installing picocom"
        sudo apt-get install -y -qq picocom && echo "All Done!"

        print_info "Start picocom by typing the following: "
        echo "picocom -b 115200 -r -l /dev/ttyUSB0"
    fi
}
export -f setup_picocom

setup_cutecom() {
    if ! which cutecom > /dev/null; then
        print_info "Installing cutecom"
        sudo apt-get install -y -qq cutecom && echo "All Done!"

        print_info "Start cutecom by typing the following: "
        echo "cutecom"
    fi
}
export -f setup_cutecom

setup_ckermit() {
    if ! which ckermit > /dev/null; then
        print_info "Installing ckermit"
        sudo apt-get install -y -qq ckermit && echo "All Done!"

        print_info "Start ckermit by typing the following: "
        echo "ckermit"
    fi
}
export -f setup_ckermit

# 检查网站状态
check_url() {
    wget --spider -q -o /dev/null --tries=1 -T 5 $1
    if [ $? -eq 0 ]
    then
        echo "$1 is yes."
    else
        echo "$1 is no."
    fi
}
export -f check_url


# 临时性文件的建立和使用
# https://blog.csdn.net/jerry_1126/article/details/52336794
#
# TMPFILE=`mktemp /tmp/${0##*/}.XXXXX`   # 产生临时性文件名
# trap 'rm -f $TMPFILE' 0                # 完成时删除临时性文件

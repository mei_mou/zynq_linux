# zynq_linux

#### 介绍
项目介绍：
zynq的linux sdk环境
致力于完全替代petalinux，方便专业嵌入式开发人员开发程序

由于本人之前基于AX7010做过一些项目，自己搭建了一个sdk开发环境，觉得可以分享出来，所以重新建立一个目录，准备搭建一个正式的SDK环境

本想搭建一个支持所有linux版本的sdk环境，但是考虑到代码维护困难，所以这里暂时选择了2018.3.版本进行开发

#### 软件架构
软件架构说明
.
├── readme
├── README.en.md
├── README.md
└── software
    └── board
        └── sdk                                        #SDK top dir
            ├── fs
            │   ├── datafs
            │   └── rootfs
            ├── kernel                    
            │   └── linux-xlnx-xilinx-v2018.3
            ├── packages                                #存放常用packages,sdk安装源都来自这里
            │   ├── busybox
            │   ├── kernel
            │   ├── others
            │   ├── rootfs
            │   ├── toolchains
            │   └── uboot
            ├── scripts                                  #整个SDK命令的函数定义
            │   ├── bootgen
            │   ├── common.sh
            │   ├── env_config
            │   ├── fun_local.sh
            │   ├── i_nstall_devicetree_tools
            │   ├── install_kernel
            │   ├── install_toolchain
            │   ├── install_uboot
            │   └── sdk_command.sh
            ├── sdk.install                                #SDK一键安装脚本
            ├── tools                                      #tools安装目录
            │   ├── others
            │   └── toolchain
            ├── uboot                                      #uboot安装目录
            │   └── u-boot-xlnx-xilinx-v2018.3
            └── workspace_usr                              #用户workspace
                ├── cur_project_name                       #存放当前工程名，如果要切换工程，可直接修改此文件
                └── hw_ax7010_linuxbase                    #工程sample，不能删除，创建工程时有依赖这里的文件。

#### 安装教程

1. ./sdk.install
2. 没有了

#### 使用说明
1.安装完成后，请重新登陆，或者手动运行 ./scripts/env_config 来配置环境变量。
2.可在任意目录执行sdk命令
sdk_create_project  <project_name>  #创建自己的工程
sdk_build_uboot -a    #全编译uboot，会替换.config文件
sdk_build_uboot       #编译uboot，增量编译
sdk_build_kernel -a   #全编译kernel，会替换.config文件
sdk_build_kernel      #编译uboot，增量编译
sdk_build_bootbin     #生成BOOT.BIN,只包含fsbl和uboot
sdk_build_bootbin -a     #生成ALL_IMAGE.BIN,包含所有image
sdk_build_dtb           #生成dtb
sdk_build_fs            #生成rootfs
   
    


#### 参与贡献

1. Fork 本仓库
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
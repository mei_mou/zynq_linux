# zynq_linux

#### Description
项目介绍：
zynq的linux sdk环境
致力于完全替代petalinux，方便专业嵌入式开发人员开发程序

由于本人之前基于AX7010做过一些项目，自己搭建了一个sdk开发环境，觉得可以分享出来，所以重新建立一个目录，准备搭建一个正式的SDK环境

本想搭建一个支持所有linux版本的sdk环境，但是考虑到代码维护困难，所以这里暂时选择了2018.3.版本进行开发

#### Software Architecture
Software architecture description

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

#### Contribution

1. Fork the repository
2. Create Feat_xxx branch
3. Commit your code
4. Create Pull Request


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)